import * as actions from '../actions/ActionTypes';

const initialState = {
  isAuthenticated: false,
  userInfo: {}
};

const AuthReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.REQUEST_INITIATED:
      return {
        ...state
      };

    default:
      return {
        ...state
      };
  }
};
export default AuthReducer;
