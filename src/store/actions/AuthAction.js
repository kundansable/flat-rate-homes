import axiosInstance from '../../common/AuthInterceptor';
import * as actionTypes from './ActionTypes';
import { API_LOGIN } from '../../constants/Api';

export const requestInitiated = () => {
  return {
    type: actionTypes.REQUEST_INITIATED
  };
};

export const userLogin = data => {
  return dispatch => {
    dispatch(requestInitiated());
    axiosInstance.post(API_LOGIN, data).catch();
  };
};
