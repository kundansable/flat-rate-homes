import React from 'react';
import Loader from 'react-loader-spinner';

export default function PageLoader() {
  return <Loader type="Rings" color="#f64225" height={100} width={100} />;
}
