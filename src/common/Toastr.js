import React from 'react';
import { ToastContainer, toast } from 'react-toastify';
import PropTypes from 'prop-types';

function Toastr(props) {
  const notify = () => {
    const { type, message } = props;
    if (type) {
      toast[type](message, {
        position: toast.POSITION.TOP_RIGHT
      });
    } else toast('Something went wrong!');
  };

  return (
    <div>
      {notify()}
      <ToastContainer />
    </div>
  );
}

Toastr.propTypes = {
  type: PropTypes.string,
  message: PropTypes.string
};

Toastr.defaultProps = {
  type: 'success',
  message: 'Default Notification'
};
export default Toastr;
