import React from 'react';
import { Route, Switch } from 'react-router-dom';

import {
  PATH_ROOT,
  PATH_LOGIN,
  PATH_ACCOUNT,
  PATH_BUYER_PROPERTY_LIST,
  PATH_ADMIN_PROPERTY_DETAIL,
  PATH_ADMIN_PROPERTY_LIST,
  PATH_BUYER_PROPERTY_DETAIL,
  PATH_HOME,
  PATH_MAKE_AN_OFFER,
  PATH_NOT_FOUND
} from './constants/index';

// Buyer
import Home from './components/Buyer/Home';
import BuyerPropertyDetails from './components/Buyer/BuyerPropertyDetails';
import MapWrapper from './components/Buyer/BuyerPropertyList';

// Admin
// import Dashboard from './components/Admin/Dashboard';
import AdminPropertyDetails from './components/Admin/AdminPropertyDetails';
import AdminPropertyList from './components/Admin/AdminPropertyList';

// Others
import Login from './components/Auth/Login';
import Account from './components/Auth/Account';
import MakeAnOffer from './components/Buyer/MakeAnOffer';
import PageNotFound from './components/Buyer/common/PageNotFound';

function App() {
  return (
    <Switch>
      {/* Buyer */}
      <Route path={PATH_HOME} exact component={Home} />
      <Route path={PATH_MAKE_AN_OFFER} exact component={MakeAnOffer} />

      <Route path={PATH_BUYER_PROPERTY_LIST} exact component={MapWrapper} />
      <Route
        path={PATH_BUYER_PROPERTY_DETAIL}
        component={BuyerPropertyDetails}
      />
      <Route path={PATH_ROOT} exact component={Home} />

      {/* Admin */}
      <Route
        path={PATH_ADMIN_PROPERTY_LIST}
        exact
        component={AdminPropertyList}
      />
      <Route
        path={PATH_ADMIN_PROPERTY_DETAIL}
        exact
        component={AdminPropertyDetails}
      />

      {/* Others */}
      <Route path={PATH_LOGIN} component={Login} />
      <Route path={PATH_ACCOUNT} component={Account} />
      <Route path={PATH_NOT_FOUND} exact component={PageNotFound} />
    </Switch>
  );
}

export default App;
