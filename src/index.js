/* eslint-disable no-undef */
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { toast, ToastContainer } from 'react-toastify';
import * as serviceWorker from './serviceWorker';

import App from './App';
import './theme/Buyer/css/style.scss';
import './theme/Admin/css/style.scss';
import AuthReducer from './store/reducers/AuthReducer';

// toastr css style
import 'react-toastify/dist/ReactToastify.css';

// configure toastr
toast.configure({
  autoClose: 8000,
  draggable: false
});

// Reducers
const rootReducer = combineReducers({
  authReducer: AuthReducer
});

// create store
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
);

require('dotenv').config();

const app = (
  <Provider store={store}>
    <BrowserRouter>
      <ToastContainer />
      <App />
    </BrowserRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
