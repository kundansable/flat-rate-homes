import axiosInstance from '../common/AuthInterceptor';
import { API_LOGIN_SOCIAL } from '../constants/Api';

export default async function getSocialLoginToken(data = null) {
  if (data && Object.keys(data).length > 0) {
    const result = await axiosInstance
      .post(API_LOGIN_SOCIAL, data)
      .then(response => {
        return response.data;
      });
    return result;
  }
  return null;
}
