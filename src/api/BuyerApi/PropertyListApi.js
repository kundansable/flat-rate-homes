import queryString from 'query-string';
import axiosInstance from '../../common/AuthInterceptor';
import { API_MLS_LISING, API_PROPERTY_DETAIL } from '../../constants/Api';

export default function getMlsListingData(param) {
  return axiosInstance
    .get(API_MLS_LISING, {
      params: param,
      paramsSerializer(params) {
        return queryString.stringify(params, { arrayFormat: 'comma' });
      }
    })
    .then(res => {
      return res;
    })
    .catch(error => {
      return error;
    });
}

export async function getPropertyDetail(id) {
  const propertyResponse = await axiosInstance
    .get(API_PROPERTY_DETAIL + id)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      return error;
    });
  return propertyResponse;
}
