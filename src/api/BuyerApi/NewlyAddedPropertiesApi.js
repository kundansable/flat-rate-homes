import axiosInstance from '../../common/AuthInterceptor';
import { API_NEWLY_ADDED_PROPERTY } from '../../constants/Api';

export default async function getNewlyAddedProperties() {
  const result = await axiosInstance
    .get(API_NEWLY_ADDED_PROPERTY)
    .then(response => {
      return response.data;
    });
  return result;
}
