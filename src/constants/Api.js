// Auth
export const API_LOGIN = '/sign-in';
export const API_LOGIN_SOCIAL = `/login/social/`;

// Buyer properties
export const API_MLS_LISING = '/listing/';
export const API_NEWLY_ADDED_PROPERTY = '/recent-property/';
export const API_PROPERTY_DETAIL = '/property-details/?listingkey=';
