// Auth
export const PATH_ROOT = '/';
export const PATH_LOGIN = '/login';
export const PATH_ACCOUNT = '/account/:provider';

// Admin
export const PATH_ADMIN_PROPERTY_LIST = '/admin-property';
export const PATH_ADMIN_PROPERTY_DETAIL = `${PATH_ADMIN_PROPERTY_LIST}/:id`;

// Buyer
export const PATH_HOME = '/home';
export const PATH_NOT_FOUND = '*';

// export const PATH_BUY_PROPERTY = '/buy-property';
export const PATH_BUYER_PROPERTY_LIST = '/property';
export const PATH_BUYER_PROPERTY_DETAIL = `${PATH_BUYER_PROPERTY_LIST}/:id`;
export const PATH_MAKE_AN_OFFER = `${PATH_BUYER_PROPERTY_DETAIL}/make-an-offer`;
