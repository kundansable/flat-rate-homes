import {
  MinAreaOptions,
  MaxAreaOptions,
  MinPriceOptions,
  MaxPriceOptions,
  TotalRoomOptions,
  TotalBedOptions,
  TotalBathRoomOptions
} from './DropDownOptions';

const MapSelectFilters = [
  {
    label: 'Min Square Feet',
    id: 'minArea',
    name: 'min_size',
    options: MinAreaOptions
  },
  {
    label: 'Max Square Feet',
    id: 'maxArea',
    name: 'max_size',
    options: MaxAreaOptions
  },
  {
    label: 'Min Price',
    id: 'minPrice',
    name: 'min_price',
    options: MinPriceOptions
  },
  {
    label: 'Max Price',
    id: 'maxPrice',
    name: 'max_price',
    options: MaxPriceOptions
  },
  {
    label: 'Min Bedrooms',
    id: 'min_bedroom',
    name: 'min_bedroom',
    options: TotalRoomOptions
  },
  {
    label: 'Max Bedrooms',
    id: 'max_bedroom',
    name: 'max_bedroom',
    options: TotalBedOptions
  },
  {
    label: 'Min Bathroom',
    id: 'min_bathroom',
    name: 'min_bathroom',
    options: TotalBathRoomOptions
  },
  {
    label: 'Max Bathroom',
    id: 'max_bathroom',
    name: 'max_bathroom',
    options: TotalBathRoomOptions
  },
  {
    label: 'Min Age',
    id: 'min_age',
    name: 'min_age',
    options: TotalRoomOptions
  },
  {
    label: 'Max Age',
    id: 'max_age',
    name: 'max_age',
    options: TotalRoomOptions
  }
];

export default MapSelectFilters;

// export const MapSortByFilters = [
//   {
//     id: 'cooling',
//     name: 'cooling',
//     label: 'Air Conditioning'
//   },
//   {
//     id: 'pool',
//     name: 'pool',
//     label: 'Swimming Pool'
//   },
//   {
//     id: 'heating',
//     name: 'heating',
//     label: 'Central Heating'
//   }
// ];
