export const MinAreaOptions = [
  { value: '1000', label: '1000' },
  { value: '1500', label: '1500' },
  { value: '2000', label: '2000' },
  { value: '3000', label: '3000' },
  { value: '4000', label: '4000' },
  { value: '5000', label: '5000' },
  { value: '6000', label: '6000' },
  { value: '7000', label: '7000' }
];

export const MaxAreaOptions = [
  { value: '7500', label: '7500' },
  { value: '8000', label: '8000' },
  { value: '9000', label: '9000' },
  { value: '10000', label: '10000' },
  { value: '15000', label: '15000' },
  { value: '19000', label: '19000' },
  { value: '20000', label: ' 20000' }
];

export const MinPriceOptions = [
  { value: '10000', label: '10000' },
  { value: '15000', label: ' 15000' },
  { value: '20000', label: ' 20000' },
  { value: '30000', label: ' 30000' },
  { value: '40000', label: ' 40000' },
  { value: '50000', label: ' 50000' },
  { value: '60000', label: ' 60000' },
  { value: '70000', label: ' 70000' },
  { value: '80000', label: ' 80000' },
  { value: '90000', label: ' 90000' },
  { value: '100000', label: ' 100000' },
  { value: '110000', label: ' 110000' },
  { value: '120000', label: ' 120000' },
  { value: '130000', label: ' 130000' },
  { value: '140000', label: ' 140000' },
  { value: '150000', label: ' 150000' }
];

export const MaxPriceOptions = [
  { value: '100000', label: '100000' },
  { value: '200000', label: '200000' },
  { value: '300000', label: '300000' },
  { value: '400000', label: '400000' },
  { value: '500000', label: '500000' },
  { value: '600000', label: '600000' },
  { value: '700000', label: '700000' },
  { value: '800000', label: '800000' },
  { value: '900000', label: '900000' },
  { value: '1000000', label: '1000000' },
  { value: '15000000', label: '15000000' },
  { value: '2000000', label: '2000000' }
];

export const AgeOfHomeOptions = [
  { value: '0 - 1 Years', label: '0 - 1 Years' },
  { value: '0 - 5 Years', label: '0 - 5 Years' },
  { value: '0 - 10 Years', label: '0 - 10 Years' },
  { value: '0 - 20 Years', label: '0 - 20 Years' },
  { value: '0 - 50 Years', label: '0 - 50 Years' },
  { value: '50 + Years', label: '50 + Years' }
];

export const TotalRoomOptions = [
  { value: '1', label: '1' },
  { value: '2', label: '2' },
  { value: '3', label: '3' },
  { value: '4', label: '4' },
  { value: '5', label: '5' }
];

export const TotalBedOptions = [
  { value: '1', label: '1' },
  { value: '2', label: '2' },
  { value: '3', label: '3' },
  { value: '4', label: '4' },
  { value: '5', label: '5' }
];

export const TotalBathRoomOptions = [
  { value: '1', label: '1' },
  { value: '2', label: '2' },
  { value: '3', label: '3' },
  { value: '4', label: '4' },
  { value: '5', label: '5' }
];

export const SortByOptions = [
  { value: 'Newest to Oldest', label: 'Date: Newest to Oldest ' },
  { value: 'Oldest to Newes', label: 'Date: Oldest to Newest' },
  { value: 'low to high', label: 'Price: Low to high' },
  { value: 'high to low', label: 'Price: high to Low' }
];
