const PropertyDetail = [
  {
    key: 'LivingArea',
    section: 'detail',
    label: 'Living Area',
    prefix: 'sq ft'
  },
  {
    key: 'BuildingAreaTotal',
    section: 'detail',
    label: 'Total Area',
    prefix: 'sq ft'
  },
  {
    key: 'YearBuilt',
    section: 'detail',
    label: 'Year Built'
  },
  {
    key: 'PropertyType',
    section: 'detail',
    label: 'Type'
  },
  {
    key: 'ArchitecturalStyle',
    section: 'detail',
    label: 'Style'
  },
  {
    key: 'BedroomsTotal',
    section: 'room',
    label: 'Bedrooms'
  },
  {
    key: 'MainLevelBedrooms',
    section: 'room',
    label: 'Main Level Bedrooms'
  },
  {
    key: 'BathroomsTotalInteger',
    section: 'room',
    label: 'Bathrooms'
  },
  {
    key: 'BathroomsHalf',
    section: 'room',
    label: 'Bathrooms Half'
  },
  {
    key: 'BathroomsThreeQuarter',
    section: 'room',
    label: 'Bathrooms Three Quarter'
  },
  {
    key: 'BathroomsPartial',
    section: 'room',
    label: 'Bathrooms Partial'
  },
  {
    key: 'BathroomsOneQuarter',
    section: 'room',
    label: 'Bathrooms One Quarter'
  },
  {
    key: 'Flooring',
    section: 'features',
    label: 'Flooring'
  },
  {
    key: 'Cooling',
    section: 'features',
    label: 'Cooling'
  },
  {
    key: 'Heating',
    section: 'features',
    label: 'Heating'
  },
  {
    key: 'ParkingFeatures',
    section: 'features',
    label: 'Parking'
  },
  {
    key: 'Basement',
    section: 'features',
    label: 'Basement'
  },
  {
    key: 'ElementarySchool',
    section: 'school',
    label: 'Elementary School'
  },
  {
    key: 'ElementarySchoolDistrict',
    section: 'school',
    label: 'Elementary School District'
  },
  {
    key: 'MiddleOrJuniorSchool',
    section: 'school',
    label: 'Middle/Junior School'
  },
  {
    key: 'MiddleOrJuniorSchoolDistrict',
    section: 'school',
    label: 'Middle/Junior School District'
  },
  {
    key: 'HighSchool',
    section: 'school',
    label: 'High School'
  },
  {
    key: 'HighSchoolDistrict',
    section: 'school',
    label: 'High School District'
  },
  {
    key: 'Zoning',
    section: 'detail',
    label: 'Zoning'
  }
];

export default PropertyDetail;
