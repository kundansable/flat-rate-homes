import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { parse as parseURL } from 'query-string';
import PageLoader from '../../common/PageLoader';
import getSocialLoginToken from '../../api/SocialAuthApi';
import actions from '../../store/actions/index';

function Account(props) {
  const [code, setCode] = useState(null);
  const dispatch = useDispatch();

  const btnClick = () => {
    dispatch(actions.getMlsListingData());
  };
  useEffect(() => {
    if (code === null) {
      const urlParams = parseURL(props.location.search);
      if (
        urlParams &&
        Object.keys(urlParams).length > 0 &&
        Object.keys(urlParams).includes('code')
      ) {
        setCode(
          urlParams.code +
            (props.location.hash && props.location.hash.length > 0
              ? props.location.hash
              : '')
        );
      } else {
        setCode('');
      }
    } else if (
      code &&
      code.length > 0 &&
      props.match &&
      props.match.params &&
      props.match.params.provider
    ) {
      const data = {
        code,
        provider: props.match.params.provider,
        redirect_uri: window.location.origin + window.location.pathname
      };
      getSocialLoginToken(data)
        .then(res => {
          localStorage.setItem('token', res.token);
          props.history.push('/');
        })
        .catch(() => {
          props.history.push('/');
        });
    }
  }, [code, props]);

  return (
    <>
      <div>accoutnsssss</div>
      <button label="btn" type="button" onClick={btnClick}>
        mls List
      </button>
      <PageLoader />
    </>
  );
}

Account.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.object,
    provider: PropTypes.string
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func
  }).isRequired,
  location: PropTypes.shape({
    hash: PropTypes.string,
    search: PropTypes.string
  }).isRequired
};

export default Account;
