import React from 'react';

export default function Login() {
  const signInWithFb = () => {
    window.location.href = `${process.env.REACT_APP_FB_URL}&client_id=${process.env.REACT_APP_FB_CLIENT_ID}&redirect_uri=${window.location.origin}/account/facebook`;
  };

  const signInWithGoogle = () => {
    window.location.href = `${process.env.REACT_APP_GOOGLE_URL}&client_id=${process.env.REACT_APP_GOOGLE_CLIENT_ID}&redirect_uri=${window.location.origin}/account/google-oauth2`;
  };

  return (
    <>
      <div className="login-container">
        <div className="login-top-navigation">
          <div className="container">
            <div className="row pt-3 mb-3">
              <div className="col-sm-4">
                <h6 className="logo-text">Oreo</h6>
              </div>
              <div className="col-sm-8 text-right">
                <ul className="login-navbar">
                  <li className="nav-item">
                    <span className="nav-link">Home</span>
                  </li>
                  <li className="nav-item">
                    <span className="nav-link">Search Result</span>
                  </li>
                  <li className="nav-item">
                    <span className="nav-link" title="Follow us on Twitter">
                      <i className="fa fa-twitter" />
                    </span>
                  </li>
                  <li className="nav-item">
                    <button type="button" onClick={signInWithFb}>
                      <span className="nav-link" title="Like us on Facebook">
                        <i className="fa fa-facebook-f" />
                      </span>
                    </button>
                  </li>
                  <li className="nav-item">
                    <button type="button" onClick={signInWithGoogle}>
                      <span className="nav-link" title="Follow us on Instagram">
                        <i className="fa fa-instagram" />
                      </span>
                    </button>
                  </li>
                  <li className="nav-item">
                    <button type="button" className="btn btn-secondary">
                      SIGN UP
                    </button>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="overlay">
          <div className="container">
            <div className="row">
              <div className="col-12">
                <div className="card-plain">
                  <form className="login-form mt-5 pt-5">
                    <div className="header">
                      <div className="logo-container">
                        <img src="" alt="" />
                      </div>
                      <h5 className="text-center">Log in</h5>
                    </div>
                    <div className="content mt-4 mb-5">
                      <div className="form-group">
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Enter User Name"
                        />
                        <span className="input-group-addon">
                          <i className="fa fa-user-circle" />
                        </span>
                      </div>
                      <div className="form-group">
                        <input
                          type="password"
                          placeholder="Password"
                          className="form-control"
                        />
                        <span className="input-group-addon">
                          <i className="fa fa-unlock-alt" />
                        </span>
                      </div>
                    </div>
                    <div className="form-footer text-center">
                      <button type="button" className="btn btn-primary w-100">
                        SIGN IN
                      </button>
                      <span className="link forgot-link">Forgot Password?</span>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <footer className="footer mt-5">
              <div className="row">
                <div className="col-6">
                  <ul className="signup-footer-nav">
                    <li>
                      <span>Contact Us</span>
                    </li>
                    <li>
                      <span>About Us</span>
                    </li>
                    <li>
                      <span>FAQ</span>
                    </li>
                  </ul>
                </div>
                <div className="col-6 text-right">
                  <div className="copyright">
                    © 2020,
                    <span>
                      Designed by <span className="link">ThemeMakker</span>
                    </span>
                  </div>
                </div>
              </div>
            </footer>
          </div>
        </div>
      </div>
    </>
  );
}
