import React from 'react';

export default function AdminPropertyDetails() {
  return (
    <>
      <div className="dashboard-container">
        <div className="row">
          <div className="col-12">
            <div className="left-sidebar">
              <div className="sidebar-header">Hello</div>
              <ul className="sidebar-navigation">
                <li>
                  {' '}
                  <span>
                    <i className="fa fa-home" />
                    Dashboard
                  </span>{' '}
                </li>
                <li>
                  {' '}
                  <span>
                    <i className="fa fa-building-o" />
                    Property
                  </span>{' '}
                </li>
                <li>
                  {' '}
                  <span>
                    <i className="fa fa-building-o" />
                    Types
                  </span>{' '}
                </li>
                <li>
                  {' '}
                  <span>
                    <i className="fa fa-user-o" />
                    Agents
                  </span>{' '}
                </li>
                <li>
                  {' '}
                  <span>
                    <i className="fa fa-briefcase" />
                    Contract
                  </span>{' '}
                </li>
                <li>
                  {' '}
                  <span>
                    <i className="fa fa-th" />
                    App
                  </span>{' '}
                </li>
                <li>
                  {' '}
                  <span>
                    <i className="fa fa-object-ungroup" />
                    Groups
                  </span>{' '}
                </li>
                <li>
                  {' '}
                  <span>
                    <i className="fa fa-file-text" />
                    File Manager
                  </span>{' '}
                </li>
                <li>
                  {' '}
                  <span>
                    <i className="fa fa-map-o" />
                    Site Location
                  </span>{' '}
                </li>
              </ul>
            </div>
            <div className="dashboard-body">
              <div className="top-navigation">
                <div className="row">
                  <div className="col-2">
                    <ul className="top-nav-list">
                      <li>
                        <i className="fa fa-calendar" />
                      </li>
                      <li>
                        <i className="fa fa-envelope" />
                      </li>
                      <li>
                        <i className="fa fa-id-card-o" />
                      </li>
                      <li>
                        <i className="fa fa-bell" />
                      </li>
                    </ul>
                  </div>
                  <div className="col-3">
                    <form className="search-block">
                      <input
                        type="text"
                        className="form-control search-input"
                        placeholder="Search..."
                      />
                      <i className="fa fa-search" />
                    </form>
                  </div>
                  <div className="col-7 text-right">
                    <ul className="top-nav-list">
                      <li>
                        <i className="fa fa-power-off" />
                      </li>
                      <li>
                        <i className="fa fa-cog" />
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
