import React from 'react';
import Logo from '../../theme/Buyer/images/logo.png';
import Listing1 from '../../theme/Buyer/images/listing-01.jpg';
import Listing2 from '../../theme/Buyer/images/listing-02.jpg';

export default function BuyProperty() {
  return (
    <>
      {/* header */}
      <header id="header-container">
        <div id="header" className="home-header">
          <div className="container">
            <div className="row">
              <div className="col-md-2">
                <div className="left-side">
                  <div id="logo">
                    <span href="index.html">
                      <img src={Logo} alt="" />
                    </span>
                  </div>
                </div>
              </div>
              {/*  <div className="col-md-5">
               <nav id="navigation" className="style-1">
                  <ul id="responsive">
                    <li>
                      <span
                        href="listings-half-map-grid-standard.html"
                        className="current"
                      >
                        Buy
                      </span>
                    </li>
                  
                  </ul>
                </nav>
              </div> */}
              {/*  <div className="col-md-5">
                <div className="right-side">
                  <div className="header-widget">
                    <span href="login-register.html" className="sign-in">
                      <i className="fa fa-user" /> Log In / Register
                    </span>
                    <button type="button" className="button border">
                      Submit Property
                    </button>
                  </div>
                </div>
              </div> */}
            </div>
          </div>
        </div>
      </header>

      {/* Banner */}
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-6">
            <div className="fs-inner-container">
              <div id="map-container">
                <div id="mapsd">
                  <iframe
                    title="abc"
                    src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d96821.92835970703!2d-73.95729268078736!3d40.68090329983023!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1584705094924!5m2!1sen!2sin"
                  />
                </div>

                <span href="#" id="geoLocation" title="Your location" />
                <ul id="mapnav-buttons" className="top">
                  <li>
                    <span href="#" id="prevpoint" title="Previous point on map">
                      Prev
                    </span>
                  </li>
                  <li>
                    <span href="#" id="nextpoint" title="Next point on mp">
                      Next
                    </span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="col-md-6">
            <div className="fs-inner-container">
              <section className="search margin-bottom-30">
                <div className="row">
                  <div className="col-md-12">
                    <div className="main-search-box no-shadow">
                      <div id="accordion">
                        <div className="card">
                          <div className="card-header" id="headingOne">
                            <h5 className="m-0">
                              <button
                                type="button"
                                className="btn btn-link p-0"
                                data-toggle="collapse"
                                data-target="#collapseOne"
                                aria-expanded="true"
                                aria-controls="collapseOne"
                              >
                                <span
                                  href="#"
                                  className="more-search-options-trigger "
                                  data-open-title="Fliter"
                                  data-close-title="Less Options"
                                />
                              </button>
                            </h5>
                          </div>

                          <div
                            id="collapseOne"
                            className="collapse"
                            aria-labelledby="headingOne"
                            data-parent="#accordion"
                          >
                            <div className="card-body">
                              <div className="more-search-options relative mt-3">
                                <div className="row with-forms">
                                  <div className="col-sm-6">
                                    <input
                                      type="text"
                                      placeholder="Enter address e.g. street, city or state"
                                      value=""
                                    />
                                  </div>

                                  <div className="col-sm-3">
                                    <select
                                      data-placeholder="Any Status"
                                      className="chosen-select-no-single"
                                    >
                                      <option>Any Status</option>
                                      <option>For Sale</option>
                                      <option>For Rent</option>
                                    </select>
                                  </div>

                                  <div className="col-sm-3">
                                    <select
                                      data-placeholder="Any Type"
                                      className="chosen-select-no-single"
                                    >
                                      <option>Any Type</option>
                                      <option>Apartments</option>
                                      <option>Houses</option>
                                      <option>Commercial</option>
                                      <option>Garages</option>
                                      <option>Lots</option>
                                    </select>
                                  </div>
                                </div>

                                <div className="row with-forms">
                                  <div className="col-sm-3">
                                    <div className="select-input disabled-first-option">
                                      <input
                                        type="text"
                                        placeholder="Min Area"
                                        data-unit="Sq Ft"
                                      />
                                      <select>
                                        <option>Min Area</option>
                                        <option>300</option>
                                        <option>400</option>
                                        <option>500</option>
                                        <option>700</option>
                                        <option>800</option>
                                        <option>1000</option>
                                        <option>1500</option>
                                      </select>
                                    </div>
                                  </div>

                                  <div className="col-sm-3">
                                    <div className="select-input disabled-first-option">
                                      <input
                                        type="text"
                                        placeholder="Max Area"
                                        data-unit="Sq Ft"
                                      />
                                      <select>
                                        <option>Max Area</option>
                                        <option>300</option>
                                        <option>400</option>
                                        <option>500</option>
                                        <option>700</option>
                                        <option>800</option>
                                        <option>1000</option>
                                        <option>1500</option>
                                      </select>
                                    </div>
                                  </div>

                                  <div className="col-sm-3">
                                    <div className="select-input disabled-first-option">
                                      <input
                                        type="text"
                                        placeholder="Min Price"
                                        data-unit="USD"
                                      />
                                      <select>
                                        <option>Min Price</option>
                                        <option>1 000</option>
                                        <option>2 000</option>
                                        <option>3 000</option>
                                        <option>4 000</option>
                                        <option>5 000</option>
                                        <option>10 000</option>
                                        <option>15 000</option>
                                        <option>20 000</option>
                                        <option>30 000</option>
                                        <option>40 000</option>
                                        <option>50 000</option>
                                        <option>60 000</option>
                                        <option>70 000</option>
                                        <option>80 000</option>
                                        <option>90 000</option>
                                        <option>100 000</option>
                                        <option>110 000</option>
                                        <option>120 000</option>
                                        <option>130 000</option>
                                        <option>140 000</option>
                                        <option>150 000</option>
                                      </select>
                                    </div>
                                  </div>

                                  <div className="col-sm-3">
                                    <div className="select-input disabled-first-option">
                                      <input
                                        type="text"
                                        placeholder="Max Price"
                                        data-unit="USD"
                                      />
                                      <select>
                                        <option>Max Price</option>
                                        <option>1 000</option>
                                        <option>2 000</option>
                                        <option>3 000</option>
                                        <option>4 000</option>
                                        <option>5 000</option>
                                        <option>10 000</option>
                                        <option>15 000</option>
                                        <option>20 000</option>
                                        <option>30 000</option>
                                        <option>40 000</option>
                                        <option>50 000</option>
                                        <option>60 000</option>
                                        <option>70 000</option>
                                        <option>80 000</option>
                                        <option>90 000</option>
                                        <option>100 000</option>
                                        <option>110 000</option>
                                        <option>120 000</option>
                                        <option>130 000</option>
                                        <option>140 000</option>
                                        <option>150 000</option>
                                      </select>
                                    </div>
                                  </div>
                                </div>

                                <button
                                  type="button"
                                  className="button fs-map-btn"
                                >
                                  Update
                                </button>

                                <div className="more-search-options-container margin-top-30">
                                  <div className="row with-forms">
                                    <div className="col-fs-3">
                                      <select
                                        data-placeholder="Age of Home"
                                        className="chosen-select-no-single"
                                      >
                                        <option label="blank" />
                                        <option>Age of Home (Any)</option>
                                        <option>0 - 1 Years</option>
                                        <option>0 - 5 Years</option>
                                        <option>0 - 10 Years</option>
                                        <option>0 - 20 Years</option>
                                        <option>0 - 50 Years</option>
                                        <option>50 + Years</option>
                                      </select>
                                    </div>

                                    <div className="col-fs-3">
                                      <select
                                        data-placeholder="Rooms"
                                        className="chosen-select-no-single"
                                      >
                                        <option label="blank" />
                                        <option>Rooms (Any)</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                      </select>
                                    </div>

                                    <div className="col-fs-3">
                                      <select
                                        data-placeholder="Beds"
                                        className="chosen-select-no-single"
                                      >
                                        <option label="blank" />
                                        <option>Beds (Any)</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                      </select>
                                    </div>

                                    <div className="col-fs-3">
                                      <select
                                        data-placeholder="Baths"
                                        className="chosen-select-no-single"
                                      >
                                        <option label="blank" />
                                        <option>Baths (Any)</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                      </select>
                                    </div>
                                  </div>

                                  <div className="checkboxes in-row">
                                    <input
                                      id="check-2"
                                      type="checkbox"
                                      name="check"
                                    />
                                    <div htmlFor="check-2">
                                      Air Conditioning{' '}
                                    </div>

                                    <input
                                      id="check-3"
                                      type="checkbox"
                                      name="check"
                                    />
                                    <div htmlFor="check-3">Swimming Pool</div>

                                    <input
                                      id="check-4"
                                      type="checkbox"
                                      name="check"
                                    />
                                    <div htmlFor="check-4">Central Heating</div>

                                    <input
                                      id="check-5"
                                      type="checkbox"
                                      name="check"
                                    />
                                    <div htmlFor="check-5">Laundry Room</div>

                                    <input
                                      id="check-6"
                                      type="checkbox"
                                      name="check"
                                    />
                                    <div htmlFor="check-6">Gym</div>

                                    <input
                                      id="check-7"
                                      type="checkbox"
                                      name="check"
                                    />
                                    <div htmlFor="check-7">Alarm</div>

                                    <input
                                      id="check-8"
                                      type="checkbox"
                                      name="check"
                                    />
                                    <div htmlFor="check-8">Window Covering</div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>

              <div className="fs-content">
                {/* <section className="search mb-3">
                  <div className="row">
                    <div className="col-md-12">
                      <h3> Apartments</h3>
                      <div>
                        <i className="fa fa-map-marker mr-2" />
                        9364 School St. Lynchburg, NY
                      </div>
                    </div>
                  </div>
                </section> */}

                <div className="row fs-switcher">
                  <div className="col-md-6">
                    <p className="showing-results">14 Results Found </p>
                  </div>

                  <div className="col-md-6">
                    <div className="layout-switcher">
                      <span href="#" className="grid">
                        <i className="fa fa-th-large" />
                      </span>
                      <span href="#" className="list">
                        <i className="fa fa-th-list" />
                      </span>
                    </div>
                  </div>
                </div>

                <div className="listings-container grid-layout row fs-listings">
                  <div className="row">
                    <div className="col-6">
                      <div className="listing-item ">
                        <span
                          href="single-property-page-1.html"
                          className="listing-img-container"
                        >
                          <div className="listing-badges">
                            <span className="featured">Featured</span>
                            <span>For Sale</span>
                          </div>

                          <div className="listing-img-content">
                            <span className="listing-price">
                              $275,000 <i>$520 / sq ft</i>
                            </span>
                            <span
                              className="like-icon with-tip"
                              data-tip-content="Add to Bookmarks"
                            />
                            <span
                              className="compare-button with-tip"
                              data-tip-content="Add to Compare"
                            />
                          </div>

                          <div className="listing-carousel">
                            <div>
                              <img src={Listing1} alt="" />
                            </div>
                          </div>
                        </span>

                        <div className="listing-content">
                          <div className="listing-title">
                            <h4>
                              <span href="single-property-page-1.html">
                                Eagle Apartments
                              </span>
                            </h4>
                            <span
                              href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&hl=en&t=v&hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom"
                              className="listing-address popup-gmaps"
                            >
                              <i className="fa fa-map-marker" />
                              9364 School St. Lynchburg, NY
                            </span>

                            <span
                              href="single-property-page-1.html"
                              className="details button border"
                            >
                              Details
                            </span>
                          </div>

                          <ul className="listing-details">
                            <li>530 sq ft</li>
                            <li>1 Bedroom</li>
                            <li>3 Rooms</li>
                            <li>1 Bathroom</li>
                          </ul>

                          <div className="listing-footer">
                            <span href="#">
                              <i className="fa fa-user" /> David Strozier
                            </span>
                            <span>
                              <i className="fa fa-calendar-o" /> 1 day ago
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-6">
                      <div className="listing-item ">
                        <span
                          href="single-property-page-1.html"
                          className="listing-img-container"
                        >
                          <div className="listing-badges">
                            <span>For Rent</span>
                          </div>

                          <div className="listing-img-content">
                            <span className="listing-price">
                              $900 <i>monthly</i>
                            </span>
                            <span
                              className="like-icon with-tip"
                              data-tip-content="Add to Bookmarks"
                            />
                            <span
                              className="compare-button with-tip"
                              data-tip-content="Add to Compare"
                            />
                          </div>

                          <img src={Listing2} alt="" />
                        </span>

                        <div className="listing-content">
                          <div className="listing-title">
                            <h4>
                              <span href="single-property-page-1.html">
                                Serene Uptown
                              </span>
                            </h4>
                            <span
                              href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&hl=en&t=v&hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom"
                              className="listing-address popup-gmaps"
                            >
                              <i className="fa fa-map-marker" />6 Bishop Ave.
                              Perkasie, PA
                            </span>

                            <span
                              href="single-property-page-1.html"
                              className="details button border"
                            >
                              Details
                            </span>
                          </div>

                          <ul className="listing-details">
                            <li>440 sq ft</li>
                            <li>1 Bedroom</li>
                            <li>1 Room</li>
                            <li>1 Bathroom</li>
                          </ul>

                          <div className="listing-footer">
                            <span href="#">
                              <i className="fa fa-user" /> Harriette Clark
                            </span>
                            <span>
                              <i className="fa fa-calendar-o" /> 2 days ago
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">
                      <div className="listing-item  ">
                        <span
                          href="single-property-page-1.html"
                          className="listing-img-container"
                        >
                          <div className="listing-badges">
                            <span className="featured">Featured</span>
                            <span>For Rent</span>
                          </div>

                          <div className="listing-img-content">
                            <span className="listing-price">
                              $1700 <i>monthly</i>
                            </span>
                            <span
                              className="like-icon with-tip"
                              data-tip-content="Add to Bookmarks"
                            />
                            <span
                              className="compare-button with-tip"
                              data-tip-content="Add to Compare"
                            />
                          </div>

                          <img src={Listing2} alt="" />
                        </span>

                        <div className="listing-content">
                          <div className="listing-title">
                            <h4>
                              <span href="single-property-page-1.html">
                                Meridian Villas
                              </span>
                            </h4>
                            <span
                              href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&hl=en&t=v&hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom"
                              className="listing-address popup-gmaps"
                            >
                              <i className="fa fa-map-marker" />
                              778 Country St. Panama City, FL
                            </span>

                            <span
                              href="single-property-page-1.html"
                              className="details button border"
                            >
                              Details
                            </span>
                          </div>

                          <ul className="listing-details">
                            <li>1450 sq ft</li>
                            <li>1 Bedroom</li>
                            <li>2 Rooms</li>
                            <li>2 Rooms</li>
                          </ul>

                          <div className="listing-footer">
                            <span href="#">
                              <i className="fa fa-user" /> Chester Miller
                            </span>
                            <span>
                              <i className="fa fa-calendar-o" /> 4 days ago
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-6">
                      <div className="listing-item  ">
                        <span
                          href="single-property-page-1.html"
                          className="listing-img-container"
                        >
                          <div className="listing-badges">
                            <span>For Sale</span>
                          </div>

                          <div className="listing-img-content">
                            <span className="listing-price">
                              $420,000 <i>$770 / sq ft</i>
                            </span>
                            <span
                              className="like-icon with-tip"
                              data-tip-content="Add to Bookmarks"
                            />
                            <span
                              className="compare-button with-tip"
                              data-tip-content="Add to Compare"
                            />
                          </div>

                          <div className="listing-carousel">
                            <div>
                              <img src={Listing1} alt="" />
                            </div>
                          </div>
                        </span>

                        <div className="listing-content">
                          <div className="listing-title">
                            <h4>
                              <span href="single-property-page-1.html">
                                Selway Apartments
                              </span>
                            </h4>
                            <span
                              href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&hl=en&t=v&hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom"
                              className="listing-address popup-gmaps"
                            >
                              <i className="fa fa-map-marker" />
                              33 William St. Northbrook, IL
                            </span>

                            <span
                              href="single-property-page-1.html"
                              className="details button border"
                            >
                              Details
                            </span>
                          </div>

                          <ul className="listing-details">
                            <li>540 sq ft</li>
                            <li>1 Bedroom</li>
                            <li>3 Rooms</li>
                            <li>2 Bathroom</li>
                          </ul>

                          <div className="listing-footer">
                            <span href="#">
                              <i className="fa fa-user" /> Kristen Berry
                            </span>
                            <span>
                              <i className="fa fa-calendar-o" /> 3 days ago
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">
                      <div className="listing-item  ">
                        <span
                          href="single-property-page-1.html"
                          className="listing-img-container"
                        >
                          <div className="listing-badges">
                            <span>For Sale</span>
                          </div>

                          <div className="listing-img-content">
                            <span className="listing-price">
                              $535,000 <i>$640 / sq ft</i>
                            </span>
                            <span
                              className="like-icon with-tip"
                              data-tip-content="Add to Bookmarks"
                            />
                            <span
                              className="compare-button with-tip"
                              data-tip-content="Add to Compare"
                            />
                          </div>

                          <img src={Listing1} alt="" />
                        </span>

                        <div className="listing-content">
                          <div className="listing-title">
                            <h4>
                              <span href="single-property-page-1.html">
                                Oak Tree Villas
                              </span>
                            </h4>
                            <span
                              href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&hl=en&t=v&hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom"
                              className="listing-address popup-gmaps"
                            >
                              <i className="fa fa-map-marker" />
                              71 Lower River Dr. Bronx, NY
                            </span>

                            <span
                              href="single-property-page-1.html"
                              className="details button border"
                            >
                              Details
                            </span>
                          </div>

                          <ul className="listing-details">
                            <li>350 sq ft</li>
                            <li>1 Bedroom</li>
                            <li>2 Rooms</li>
                            <li>1 Bathroom</li>
                          </ul>

                          <div className="listing-footer">
                            <span href="#">
                              <i className="fa fa-user" /> Mabel Gagnon
                            </span>
                            <span>
                              <i className="fa fa-calendar-o" /> 4 days ago
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-6">
                      <div className="listing-item  ">
                        <span
                          href="single-property-page-1.html"
                          className="listing-img-container"
                        >
                          <div className="listing-badges">
                            <span>For Rent</span>
                          </div>

                          <div className="listing-img-content">
                            <span className="listing-price">
                              $500 <i>monthly</i>
                            </span>
                            <span
                              className="like-icon with-tip"
                              data-tip-content="Add to Bookmarks"
                            />
                            <span
                              className="compare-button with-tip"
                              data-tip-content="Add to Compare"
                            />
                          </div>

                          <img src={Listing1} alt="" />
                        </span>

                        <div className="listing-content">
                          <div className="listing-title">
                            <h4>
                              <span href="single-property-page-1.html">
                                Old Town Manchester
                              </span>
                            </h4>
                            <span
                              href="https://maps.google.com/maps?q=221B+Baker+Street,+London,+United+Kingdom&hl=en&t=v&hnear=221B+Baker+St,+London+NW1+6XE,+United+Kingdom"
                              className="listing-address popup-gmaps"
                            >
                              <i className="fa fa-map-marker" />
                              7843 Durham Avenue, MD
                            </span>

                            <span
                              href="single-property-page-1.html"
                              className="details button border"
                            >
                              Details
                            </span>
                          </div>

                          <ul className="listing-details">
                            <li>850 sq ft</li>
                            <li>2 Bedroom</li>
                            <li>3 Rooms</li>
                            <li>1 Bathroom</li>
                          </ul>

                          <div className="listing-footer">
                            <span href="#">
                              <i className="fa fa-user" /> Charles Watson
                            </span>
                            <span>
                              <i className="fa fa-calendar-o" /> 3 days ago
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="row fs-listings">
                  <div className="col-md-12">
                    <div className="clearfix" />
                    <div className="pagination-container margin-top-10 margin-bottom-45">
                      <nav className="pagination  text-center ml-auto">
                        <ul>
                          <li>
                            <span href="#" className="current-page">
                              1
                            </span>
                          </li>
                          <li>
                            <span href="#">2</span>
                          </li>
                          <li>
                            <span href="#">3</span>
                          </li>
                          <li className="blank">...</li>
                          <li>
                            <span href="#">22</span>
                          </li>
                        </ul>
                      </nav>

                      <nav className="pagination-next-prev">
                        <ul>
                          <li>
                            <span href="#" className="prev">
                              Previous
                            </span>
                          </li>
                          <li>
                            <span href="#" className="next">
                              Next
                            </span>
                          </li>
                        </ul>
                      </nav>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
