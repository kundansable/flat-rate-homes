import React from 'react';
import PropTypes from 'prop-types';

import ImageGallery from 'react-image-gallery';
import NumberFormat from 'react-number-format';
import moment from 'moment';
import ReCAPTCHA from 'react-google-recaptcha';

import Header from './common/Header';
import Footer from './common/Footer';

export default function MakeAnOffer(props) {
  const { location } = props;
  const {
    price,
    area,
    address,
    bathrooms,
    bedrooms,
    streetName,
    media,
    agentName,
    date,
    mlsid
  } = location.state;

  const getImages = () => {
    const images = [];

    if (media) {
      media.forEach(img => {
        images.push({
          original: img.MediaURL
        });
      });
    }
    return images;
  };

  return (
    <>
      <Header />
      <section className="search mb-0 make-an-offer pt-5">
        <div className="container">
          <div className="row">
            <div className="col-md-5">
              <div className="carousel-item">
                <div className="listing-item">
                  <ImageGallery items={getImages()} showThumbnails={false} />
                  <span className="listing-img-container">
                    <div className="listing-img-content">
                      <span className="listing-price">
                        <NumberFormat
                          defaultValue={price || 'N/A'}
                          displayType="text"
                          thousandSeparator
                          prefix="$"
                        />
                      </span>
                    </div>
                  </span>

                  <div className="listing-content">
                    <div className="listing-title">
                      <h4>
                        <span href="single-property-page-1.html">
                          {streetName || 'N/A'}
                        </span>
                      </h4>
                      <span className="listing-address popup-gmaps">
                        <i className="fa fa-map-marker" />
                        {address || 'N/A'}
                      </span>
                    </div>

                    <ul className="listing-features">
                      <li>
                        Area <span> {area || 'N/A'} sq ft</span>
                      </li>
                      <li>
                        Bedrooms <span> {bedrooms || 'N/A'}</span>
                      </li>
                      <li>
                        Bathrooms <span> {bathrooms || 'N/A'}</span>
                      </li>
                    </ul>

                    <div className="listing-footer">
                      <div className="row">
                        <div className="col-6">
                          <span href="#">
                            <i className="fa fa-user" /> {agentName || 'N/A'}
                          </span>
                        </div>
                        <div className="col-6 text-right">
                          <span>
                            <i className="fa fa-calendar-o" />{' '}
                            {moment(date).fromNow() || 'N/A'}
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-7">
              <h3 className="search-title mt-2">Make An Offer</h3>

              <form
                name="insightly_web_to_lead"
                action="https://crm.na1.insightly.com/WebToLead/Create"
                method="post"
              >
                <input
                  type="hidden"
                  name="formId"
                  defaultValue="3jtuuHjkntCoE3gC7AIPMg=="
                />
                <div className=" no-shadow">
                  <div className="row form-group ">
                    <div className="col-sm-12">
                      <div className="label-text" htmlFor="name">
                        First Name:
                      </div>
                      <input
                        name="FirstName"
                        type="text"
                        className="ico-01"
                        placeholder="Enter First Name"
                        defaultValue=""
                      />
                    </div>
                  </div>
                  <div className="row form-group ">
                    <div className="col-sm-12">
                      <div className="label-text" htmlFor="name">
                        Last Name:
                      </div>
                      <input
                        name="LastName"
                        type="text"
                        className="ico-01"
                        placeholder="Enter Last Name"
                        defaultValue=""
                      />
                    </div>
                  </div>
                  <div className="row form-group">
                    <div className="col-sm-12">
                      <div className="label-text" htmlFor="name">
                        Email:
                      </div>
                      <input
                        name="email"
                        type="mail"
                        className="ico-01"
                        placeholder="Enter Email ID"
                        defaultValue=""
                      />
                    </div>
                  </div>

                  <div className="row form-group">
                    <div className="col-sm-12">
                      <div className="label-text" htmlFor="name">
                        Phone No:
                      </div>
                      <input
                        name="phone"
                        type="text"
                        className="ico-01"
                        placeholder="Enter Phone no"
                        defaultValue=""
                      />
                    </div>
                  </div>

                  <div className="row form-group">
                    <div className="col-sm-12">
                      <div className="label-text" htmlFor="name">
                        MLS ID:
                      </div>
                      <input
                        type="text"
                        className="ico-01"
                        placeholder="Enter MLS ID"
                        defaultValue={mlsid}
                        disabled
                      />
                    </div>
                  </div>
                  <div className="row form-group">
                    <div className="col-sm-12">
                      <div className="label-text" htmlFor="name">
                        Address:
                      </div>
                      <input
                        name="What_is_the_address_of_the_property_for_which_you_want_to_submit_an_offer__c"
                        type="text"
                        className="ico-01"
                        placeholder="Enter Address"
                        defaultValue={address}
                      />
                    </div>
                  </div>

                  <div className="row form-group">
                    <div className="col-sm-12">
                      <div className="label-text" htmlFor="name">
                        How did you hear about us?
                      </div>
                      <input
                        name="How_did_you_hear_about_us__c"
                        type="text"
                        className="ico-01"
                        placeholder="How did you hear about us?"
                        defaultValue=""
                      />
                    </div>
                  </div>

                  <div className="row form-group">
                    <div className="col-sm-12">
                      <div className="label-text" htmlFor="name">
                        Who Referred you to us? (if referral)
                      </div>
                      <input
                        name="Who_Referred_you_to_us_if_referral__c"
                        type="text"
                        className="ico-01"
                        placeholder="Who Referred you to us?"
                        defaultValue=""
                      />
                    </div>
                  </div>
                  <div className="row form-group">
                    <div className="col-sm-12">
                      <div className="label-text" htmlFor="name">
                        If you are buying the property with someone else, please
                        list their name(s).
                      </div>
                      <input
                        name="If_you_are_buying_the_home_with_someone_else_please_list_their_names__c"
                        type="text"
                        className="ico-01"
                        placeholder="Property with someone else"
                        defaultValue=""
                      />
                    </div>
                  </div>

                  <div className="row form-group">
                    <div className="col-md-12">
                      <div className="label-text">
                        Have you been pre-approved for a mortgage loan?
                      </div>

                      <div className="">
                        <input
                          type="radio"
                          name="Have_you_been_prequalified_for_a_mortgage_loan__c"
                          className="radio-input"
                        />
                        &nbsp;&nbsp;Yes
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input
                          type="radio"
                          name="Have_you_been_prequalified_for_a_mortgage_loan__c"
                          className="radio-input"
                        />
                        &nbsp;&nbsp;No &nbsp;&nbsp;&nbsp;
                      </div>
                    </div>
                  </div>
                  <div className="row form-group">
                    <div className="col-md-12">
                      <div className="label-text">
                        Is your offer subject to you selling your current
                        property?
                      </div>

                      <div className="">
                        <input
                          type="radio"
                          name="Is_your_offer_subject_to_you_selling_your_current_home__c"
                          className="radio-input"
                        />
                        &nbsp;&nbsp;Yes
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input
                          type="radio"
                          name="Is_your_offer_subject_to_you_selling_your_current_home__c"
                          className="radio-input"
                        />
                        &nbsp;&nbsp;No &nbsp;&nbsp;&nbsp;
                      </div>
                    </div>
                  </div>
                  <div className="row form-group">
                    <div className="col-md-12">
                      <div className="label-text">
                        Do you already have a contract with a buying agent?
                      </div>

                      <div className="">
                        <input
                          type="radio"
                          className="radio-input"
                          name="Do_you_already_have_a_contract_with_a_buying_agent__c"
                        />
                        &nbsp;&nbsp; Yes
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input
                          type="radio"
                          name="Do_you_already_have_a_contract_with_a_buying_agent__c"
                          className="radio-input"
                        />
                        &nbsp;&nbsp;No &nbsp;&nbsp;&nbsp;
                      </div>
                    </div>
                  </div>

                  <input
                    type="hidden"
                    id="insightly_ResponsibleUser"
                    name="ResponsibleUser"
                    defaultValue="1178766"
                  />
                  <input
                    type="hidden"
                    id="insightly_LeadSource"
                    name="LeadSource"
                    defaultValue="3154877"
                  />
                  <ReCAPTCHA
                    className="g-recaptcha"
                    sitekey="6Lfcs-MUAAAAAFw7lTSbJWKNNHq4jdwUJvupiPON"
                  />

                  <button className="mt-3 button with-bgcolor" type="submit">
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
}

MakeAnOffer.propTypes = {
  location: PropTypes.shape({
    state: PropTypes.object
  }).isRequired
};
