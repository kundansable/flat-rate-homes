import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useParams } from 'react-router';

import ImageGallery from 'react-image-gallery';
import NumberFormat from 'react-number-format';
import ShowMoreText from 'react-show-more-text';

import Header from './common/Header';
import { getPropertyDetail } from '../../api/BuyerApi/PropertyListApi';
import PageLoader from '../../common/PageLoader';
import SingleMapDetail from './SingleMapDetail';
import PropertyDetail from '../../constants/Buyer/PropertyDetail';
import { PATH_MAKE_AN_OFFER } from '../../constants/index';
import Footer from './common/Footer';

export default function BuyerPropertyDetails() {
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState({});
  const { id } = useParams();

  const getDetails = () => {
    getPropertyDetail(id).then(response => {
      setData(response);
      setIsLoading(false);
    });
  };

  useEffect(() => {
    if (isLoading && data && Object.keys(data).length === 0) {
      getDetails();
    }
    // eslint-disable-next-line
  }, [isLoading, data]);

  const getImages = () => {
    const images = [];

    if (data && data.Media) {
      data.Media.forEach(img => {
        images.push({
          original: img.MediaURL,
          thumbnail: img.MediaURL
        });
      });
    }
    return images;
  };

  return (
    <>
      <Header />
      {isLoading ? (
        <div className="loader-outer">
          <PageLoader />
        </div>
      ) : (
        <div className="container property-details">
          <div className="row">
            {/* Left side */}
            <div className="col-lg-8 col-md-7 sp-content mt-5">
              <div className="property-image-slider">
                <ImageGallery items={getImages()} />
              </div>

              <div className="property-description mt-4">
                <ul className="property-main-features">
                  <li>
                    Area <span>{data.LivingArea || 'N/A'} sq ft</span>
                  </li>
                  <li>
                    Rooms <span>{data.RoomsTotal || 'N/A'}</span>
                  </li>
                  <li>
                    Bedrooms <span>{data.BedroomsTotal || 'N/A'}</span>
                  </li>
                  <li>
                    Bathrooms <span>{data.BathroomsTotalInteger || 'N/A'}</span>
                  </li>
                </ul>
                <div className="widget d-md-none d-block">
                  <div
                    id="booking-widget-anchor"
                    className="boxed-widget booking-widget "
                  >
                    <Link
                      to={{
                        pathname: PATH_MAKE_AN_OFFER.replace(':id', id),
                        state: {
                          price: data.price,
                          area: data.LivingArea,
                          address: data.Address,
                          bedrooms: data.BedroomsTotal,
                          bathrooms: data.BathroomsTotalInteger,
                          streetName: data.StreetName,
                          media: data.Media,
                          agentName: data.ListAgentFullName,
                          date: data.ModificationTimestamp,
                          mlsid: id
                        }
                      }}
                    >
                      <div className="button book-now fullwidth d-block text-center">
                        Make An Offer
                      </div>
                    </Link>
                    {/* <div className="button like-button fullwidth d-block mt-3 text-center">
                      <span className="like-icon" /> &nbsp; Favourite
                    </div> */}
                  </div>
                </div>
                <h3 className="desc-headline">
                  <i className="im im-icon-File-HorizontalText mr-2" />{' '}
                  Description
                </h3>

                <div className="">
                  <ShowMoreText
                    more="Show more"
                    less="Show less"
                    anchorClass="show-more-less"
                    expanded={false}
                  >
                    <p>{data.PublicRemarks || 'N/A'}</p>
                  </ShowMoreText>

                  {/* <span className="show-more-button">
                    Show More <i className="fa fa-angle-down" />
                  </span> */}
                </div>
                <h3 className="desc-headline">
                  <i className="im im-icon-Hotel mr-2" />
                  Property Details
                </h3>
                <ul className="property-features margin-top-0">
                  {PropertyDetail.filter(section =>
                    section.section.includes('detail')
                  ).map(item => (
                    <li key={item.key}>
                      {item.label}{' '}
                      <span>
                        {(data && data[item.key]) || 'N/A'} {item.prefix}
                      </span>
                    </li>
                  ))}
                </ul>
                <hr />

                <h3 className="desc-headline">
                  <i className="im im-icon-Shop mr-2" />
                  Rooms
                </h3>
                <ul className="property-features margin-top-0">
                  {PropertyDetail.filter(section =>
                    section.section.includes('room')
                  ).map(item => (
                    <li key={item.key}>
                      {item.label}{' '}
                      <span>{(data && data[item.key]) || 'N/A'}</span>
                    </li>
                  ))}
                </ul>

                <hr />
                <h3 className="desc-headline">
                  <i className="im im-icon-Bulleted-List mr-2" />
                  Features
                </h3>
                <ul className="property-features margin-top-0">
                  {PropertyDetail.filter(section =>
                    section.section.includes('features')
                  ).map(item => (
                    <li key={item.key}>
                      {item.label}{' '}
                      <span>{(data && data[item.key]) || 'N/A'}</span>
                    </li>
                  ))}
                </ul>

                <hr />
                <h3 className="desc-headline">
                  <i className="im im-icon-Home-2 mr-2" />
                  Interior Features
                </h3>
                <ul className="property-features margin-top-0">
                  {(data.InteriorFeatures &&
                    data.InteriorFeatures.split(',').map(element => {
                      return <li key={Math.random()}>{element}</li>;
                    })) ||
                    'N/A'}
                </ul>

                <hr />
                <h3 className="desc-headline">
                  <i className="im im-icon-Building mr-2" />
                  Exterior Features
                </h3>
                <ul className="property-features margin-top-0">
                  {(data.ExteriorFeatures &&
                    data.ExteriorFeatures.split(',').map(element => {
                      return <li key={Math.random()}>{element}</li>;
                    })) ||
                    'N/A'}
                </ul>

                <hr />
                <h3 className="desc-headline">
                  <i className="im im-icon-Handshake mr-2" />
                  Inclusions
                </h3>
                <ul className="property-features margin-top-0">
                  {(data.Inclusions &&
                    data.Inclusions.split(',').map(element => {
                      return <li key={Math.random()}>{element}</li>;
                    })) ||
                    'N/A'}
                </ul>

                <hr />
                <h3 className="desc-headline">
                  <i className="im im-icon-Checked-User mr-2" />
                  Accessibility Features{' '}
                </h3>
                <ul className="property-features margin-top-0">
                  {(data.AccessibilityFeatures &&
                    data.AccessibilityFeatures.split(',').map(element => {
                      return <li key={Math.random()}>{element}</li>;
                    })) ||
                    'N/A'}
                </ul>

                <hr />
                <h3 className="desc-headline">
                  <i className="im im-icon-Wrench mr-2" />
                  Utilities{' '}
                </h3>
                <ul className="property-features margin-top-0">
                  {(data.Utilities &&
                    data.Utilities.split(',').map(element => {
                      return <li key={Math.random()}>{element}</li>;
                    })) ||
                    'N/A'}
                </ul>

                <hr />
                <h3 className="desc-headline">
                  <i className="im im-icon-Money-2 mr-2" />
                  Association Fee Includes
                </h3>
                <ul className="property-features margin-top-0">
                  {(data.AssociationFeeIncludes &&
                    data.AssociationFeeIncludes.split(',').map(element => {
                      return <li key={Math.random()}>{element}</li>;
                    })) ||
                    'N/A'}
                </ul>

                <h3 className="desc-headline">
                  <i className="im im-icon-Swimming mr-2" />
                  Amenities
                </h3>
                <ul className="property-features checkboxes margin-top-0">
                  {(data.AssociationAmenities &&
                    data.AssociationAmenities.split(',').map(element => {
                      return <li key={Math.random()}>{element}</li>;
                    })) ||
                    'N/A'}
                </ul>

                <h3 className="desc-headline">
                  <i className="im im-icon-University mr-2" />
                  Schools
                </h3>
                <ul className="property-features margin-top-0">
                  {PropertyDetail.filter(section =>
                    section.section.includes('school')
                  ).map(item => (
                    <li key={item.key}>
                      {item.label}{' '}
                      <span>
                        {(data && data[item.key]) || 'N/A'} {item.prefix}
                      </span>
                    </li>
                  ))}
                </ul>

                <h3 className="desc-headline">
                  <i className="im im-icon-Location-2 mr-2" />
                  Location
                </h3>
                <SingleMapDetail
                  address={data.Address || 'N/A'}
                  zip={data.PostalCode || 'N/A'}
                  state={data.StateOrProvince || 'N/A'}
                  city={data.City || 'N/A'}
                  lat={data.GeoLocation.latitude || 'N/A'}
                  lng={data.GeoLocation.longitude || 'N/A'}
                />

                {/* <h3 className="desc-headline"><i className="im im-icon-Globe mr-2" />Similar Properties</h3> */}
                {/* <div className="listings-container list-layout">
                  <div className="row">
                    <div className="col-md-12  sp-content">
                      <div className="listing-item">
                        <div className="listing-img-container">
                          <div className="listing-img-content">
                            <span className="listing-price">$170000 </span>
                            <span className="like-icon" />
                          </div>

                          <img src={ProperyImg} alt="" />
                        </div>

                        <div className="listing-content">
                          <div className="listing-title">
                            <h4>
                              <span>Meridian Villas</span>
                            </h4>
                            <span className="listing-address popup-gmaps">
                              <i className="fa fa-map-marker" />
                              778 Country St. Panama City, FL
                            </span>

                            <span className="details button border">
                              Details
                            </span>
                          </div>

                          <ul className="listing-details">
                            <li>1450 sq ft</li>
                            <li>1 Bedroom</li>
                            <li>2 Rooms</li>
                            <li>2 Rooms</li>
                          </ul>

                          <div className="listing-footer">
                            <span>
                              <i className="fa fa-calendar-o" /> 4 days ago
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> */}
              </div>
            </div>

            {/* Sidebar */}
            <div className="col-lg-4 col-md-5 sp-sidebar pt-4 d-md-block d-none">
              <div className="sidebar sticky-top">
                <div
                  id="titlebar"
                  className="property-titlebar margin-bottom-0"
                >
                  <div className="container-fluid">
                    <div className="row">
                      <div className="col-md-12">
                        <div className="property-title">
                          <h2>{data.StreetName}</h2>
                          <span>
                            <span className="listing-address">
                              <i className="fa fa-map-marker" />
                              {data.Address}
                            </span>
                          </span>
                        </div>

                        <div className="property-pricing">
                          <div className="property-price">
                            <NumberFormat
                              value={data.ListPrice}
                              displayType="text"
                              thousandSeparator
                              prefix="$"
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="widget">
                  <div
                    id="booking-widget-anchor"
                    className="boxed-widget booking-widget "
                  >
                    <Link
                      to={{
                        pathname: PATH_MAKE_AN_OFFER.replace(':id', id),
                        state: {
                          price: data.price,
                          area: data.LivingArea,
                          address: data.Address,
                          bedrooms: data.BedroomsTotal,
                          bathrooms: data.BathroomsTotalInteger,
                          streetName: data.StreetName,
                          media: data.Media,
                          agentName: data.ListAgentFullName,
                          date: data.ModificationTimestamp,
                          mlsid: id
                        }
                      }}
                    >
                      <div className="button book-now fullwidth d-block text-center">
                        Make An Offer
                      </div>
                    </Link>
                    {/* <div className="button like-button fullwidth d-block mt-3 text-center">
                      <span className="like-icon" /> &nbsp; Favourite
                    </div> */}
                  </div>
                </div>
              </div>
            </div>
            {/* Sidebar end */}
          </div>
        </div>
      )}
      <Footer />
    </>
  );
}
