import React, { useState } from 'react';

import ItemsCarousel from 'react-items-carousel';

import Listing1 from '../../theme/Buyer/images/listing-01.jpg';
import Listing2 from '../../theme/Buyer/images/listing-02.jpg';
import Listing3 from '../../theme/Buyer/images/listing-03.jpg';

export default function FeaturedHome() {
  const [activeItemIndex, setActiveItemIndex] = useState(0);

  return (
    <>
      <ItemsCarousel
        className="padding-bottom-30"
        infiniteLoop
        requestToChangeActive={setActiveItemIndex}
        activeItemIndex={activeItemIndex}
        numberOfCards={3}
        gutter={20}
        leftChevron={
          <button type="button" className="left-arrowd">
            {'<'}
          </button>
        }
        rightChevron={
          <button type="button" className="right-arrowd">
            {'>'}
          </button>
        }
      >
        <div className="carousel home-carousel">
          <div className="carousel-item">
            <div className="listing-item">
              <span
                href="single-property-page-1.html"
                className="listing-img-container"
              >
                <div className="listing-img-content">
                  <span className="listing-price">$275,000</span>
                </div>

                <div className="listing-carousel">
                  <div>
                    <img src={Listing1} alt="" />
                  </div>
                </div>
              </span>

              <div className="listing-content">
                <div className="listing-title">
                  <h4>
                    <span href="single-property-page-1.html" target="_BLANK">
                      Eagle Apartments
                    </span>
                  </h4>
                  <span className="listing-address popup-gmaps">
                    <i className="fa fa-map-marker" />
                    9364 School St. Lynchburg, NY
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="carousel home-carousel">
          <div className="carousel-item">
            <div className="listing-item">
              <span
                href="single-property-page-1.html"
                className="listing-img-container"
              >
                <div className="listing-img-content">
                  <span className="listing-price">$275,000</span>
                </div>

                <div className="listing-carousel">
                  <div>
                    <img src={Listing2} alt="" />
                  </div>
                </div>
              </span>

              <div className="listing-content">
                <div className="listing-title">
                  <h4>
                    <span href="single-property-page-1.html" target="_BLANK">
                      Eagle Apartments
                    </span>
                  </h4>
                  <span className="listing-address popup-gmaps">
                    <i className="fa fa-map-marker" />
                    9364 School St. Lynchburg, NY
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="carousel home-carousel">
          <div className="carousel-item">
            <div className="listing-item">
              <span
                href="single-property-page-1.html"
                className="listing-img-container"
              >
                <div className="listing-img-content">
                  <span className="listing-price">$275,000</span>
                </div>

                <div className="listing-carousel">
                  <div>
                    <img src={Listing3} alt="" />
                  </div>
                </div>
              </span>

              <div className="listing-content">
                <div className="listing-title">
                  <h4>
                    <span href="single-property-page-1.html" target="_BLANK">
                      Eagle Apartments
                    </span>
                  </h4>
                  <span className="listing-address popup-gmaps">
                    <i className="fa fa-map-marker" />
                    9364 School St. Lynchburg, NY
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </ItemsCarousel>
    </>
  );
}
