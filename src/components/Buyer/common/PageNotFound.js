import React from 'react';
import { Link } from 'react-router-dom';

import Listing1 from '../../../theme/Buyer/images/robo-404.png';
import Header from './Header';
import { PATH_HOME } from '../../../constants';

export default function PageNotFound() {
  return (
    <>
      <Header />
      <div className="container mt-5 pt-5">
        <div className="row">
          <div className="col-12 text-center">
            <img src={Listing1} alt="" className="w-25" />
            <h2 className="mt-4"> Oops &#33; Page Not Found</h2>
            <Link to={PATH_HOME}>
              <button type="button" className="button mt-3 with-bgcolor">
                Back to Home
              </button>
            </Link>
          </div>
        </div>
      </div>
    </>
  );
}
