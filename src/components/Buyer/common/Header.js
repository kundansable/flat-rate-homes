import React from 'react';
import { Link } from 'react-router-dom';

import Logo from '../../../theme/Buyer/images/logo.png';
import { PATH_ROOT } from '../../../constants';

export default function Header() {
  return (
    <>
      {/* header */}
      <header id="header-container">
        <div id="header" className="home-header">
          <div className="container">
            <div className="row">
              <div className="col-lg-3  col-6">
                <div id="logo">
                  <span href="index.html">
                    <Link to={PATH_ROOT}>
                      <img src={Logo} alt="logo" />
                    </Link>
                  </span>
                </div>
              </div>
              <div className="col-lg-9   col-6 text-right">
                <input type="checkbox" id="menuToggle" />
                <label htmlFor="menuToggle" className="menu-icon">
                  <i className="fa fa-bars" />
                </label>

                <nav id="navigation" className="style-1">
                  <ul id="responsive">
                    <li>
                      <a href="https://platform.flatratehomes.com/">
                        Home Search
                      </a>
                    </li>
                    <li>
                      <a href="https://platform.flatratehomes.com/">
                        Buyer Services
                      </a>
                    </li>
                    <li>
                      <a href="https://flatratehomes.com/plans/">
                        Seller Services
                      </a>
                    </li>
                    <li>
                      <a href="https://flatratehomes.com/reviews/">Reviews</a>
                    </li>
                    <li>
                      <a href="https://flatratehomes.com/faq/">FAQ’s</a>
                    </li>
                    <li>
                      <a href="https://flatratehomes.com/login/">Login</a>
                    </li>
                    <li>
                      <a href="https://flatratehomes.com/wp-login.php?action=register">
                        Register
                      </a>
                    </li>
                    <li>
                      <a href="https://flatratehomes.com/contact/">Contact</a>
                    </li>
                  </ul>
                </nav>
                <div className="clearfix">&nbsp;</div>
              </div>
            </div>
          </div>
        </div>
      </header>
    </>
  );
}
