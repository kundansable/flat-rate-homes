import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import { Autocomplete } from '@loadup/react-google-places-autocomplete';
import { PATH_BUYER_PROPERTY_LIST } from '../../../constants';

function SearchPropertyBtn(props) {
  const history = useHistory();
  const [address, setAddress] = useState('');
  const [emptyField, setEmptyField] = useState(true);

  const handlePlaceChanged = original => {
    setAddress(original);
    setEmptyField(false);
  };

  const redirectToList = () => {
    const stringified = queryString.stringify({
      q: address.formatted_address,
      latitude:
        address &&
        address.geometry &&
        address.geometry.location &&
        address.geometry.location.lat(),
      longitude:
        address &&
        address.geometry &&
        address.geometry.location &&
        address.geometry.location.lng()
    });

    history.push({
      pathname: `${PATH_BUYER_PROPERTY_LIST}`,
      search: `${stringified}`
    });
    if (props.fetchResult) props.fetchResult();
  };

  return (
    <div className="main-search-form">
      <div className="main-search-box">
        <div className="main-search-input larger-input mb-0">
          <Autocomplete
            className="ico-01 mb-0"
            placeholder="Enter address e.g. street, city and state or zip"
            fields={['address_components', 'formatted_address', 'geometry']}
            id="example-autocomplete-id autocomplete-input"
            onPlaceChanged={({ original }) => {
              handlePlaceChanged(original);
            }}
            onChange={() => setEmptyField(true)}
            types={['(regions)']}
          />
          <button
            type="button"
            className="button with-bgcolor"
            onClick={() => redirectToList()}
            disabled={address === '' || emptyField}
          >
            Search
          </button>
        </div>
      </div>
    </div>
  );
}

SearchPropertyBtn.propTypes = {
  fetchResult: PropTypes.func
};
SearchPropertyBtn.defaultProps = {
  fetchResult: () => {}
};
export default SearchPropertyBtn;
