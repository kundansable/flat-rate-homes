import React from 'react';

import moment from 'moment';

import Logo from '../../../theme/Buyer/images/logo.png';

export default function Footer() {
  return (
    <>
      <div id="footer">
        <div className="container">
          <div className="row">
            <div className="col-md-5 col-sm-6">
              <img className="footer-logo" src={Logo} alt="logo" />
              <br />
              <br />
              <p>
                Technology continues to make it easier and easier for real
                estate agents to sell a home, yet agents are still charging
                thousands of dollars in commissions when technology is really
                doing all the work.
              </p>
            </div>

            <div className="col-md-4 col-sm-6 ">
              <h4>Helpful Links</h4>
              <ul className="footer-links">
                <li>
                  <a href="https://flatratehomes.com/login/">Login</a>
                </li>
                <li>
                  <a href="https://flatratehomes.com/login/">My Dashboard</a>
                </li>
                <li>
                  <a href="https://flatratehomes.com/">Profile</a>
                </li>

                <li>
                  <a href="https://flatratehomes.com/wp-login.php?action=register">
                    Register
                  </a>
                </li>
                <li>
                  <a href="https://platform.flatratehomes.com/">Listing</a>
                </li>
              </ul>

              <ul className="footer-links">
                <li>
                  <a href="https://platform.flatratehomes.com/">
                    Buyer Services
                  </a>
                </li>
                <li>
                  <a href="https://flatratehomes.com/plans/">
                    Seller Services &amp; Pricing
                  </a>
                </li>
                <li>
                  <a href="https://flatratehomes.com/faq/">Seller FAQ&apos;s</a>
                </li>
                <li>
                  <a href="https://flatratehomes.com/contact/">Contact</a>
                </li>
                <li>
                  <a href="https://flatratehomes.com/employment/">Employment</a>
                </li>
                <li>
                  <a href="https://flatratehomes.com/privacy-policy/">
                    Privacy Policy
                  </a>
                </li>
                <li>
                  <a href="https://flatratehomes.com/terms-of-use/">
                    Terms of Use
                  </a>
                </li>
              </ul>
              <div className="clearfix" />
            </div>

            <div className="col-md-3  col-sm-12">
              <h4>Contact Us</h4>
              <div className="text-widget">
                <span>66 E State St Pleasant Grove, UT 84062</span> <br />
                Phone: <span>801-441-7283</span>
                <br />
                E-Mail:
                <span>
                  {' '}
                  <a href="mailto:Sales@flatratehomes.com">
                    Sales@flatratehomes.com
                  </a>{' '}
                </span>
                <br />
              </div>

              <ul className="social-icons margin-top-20">
                <li>
                  <a
                    className="facebook"
                    href="https://www.facebook.com/flatratehomes"
                    target="BLANK"
                  >
                    <i className="icon-facebook" />
                  </a>
                </li>
              </ul>
            </div>
          </div>

          <div className="row">
            <div className="col-md-12">
              <div className="copyrights">
                © {moment().year() || '2020'} FlatRateHomes. All Rights
                Reserved.
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
