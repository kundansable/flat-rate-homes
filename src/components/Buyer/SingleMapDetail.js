import React from 'react';
import PropTypes from 'prop-types';

import { GoogleApiWrapper, Map, Marker } from 'google-maps-react';

const mapStyles = {
  width: '100%',
  height: '100%'
};

function SingleMapDetail(props) {
  const { google, address, state, zip, city, lat, lng } = props;
  return (
    <>
      <div className="address">
        Address: <span>{address}</span>
      </div>
      <ul className="property-features margin-top-0">
        <li>
          Country: <span>United States</span>
        </li>
        <li>
          State/county: <span>{state}</span>
        </li>
        <li>
          Zip/Postal Code: <span>{zip}</span>
        </li>
        <li>
          City: <span>{city}</span>
        </li>
      </ul>
      <div className="responsive-iframe mb-5">
        <Map
          google={google}
          zoom={14}
          initialCenter={{
            lat,
            lng
          }}
          style={mapStyles}
        >
          <Marker
            title={address}
            position={{
              lat,
              lng
            }}
          />
        </Map>
      </div>
    </>
  );
}

SingleMapDetail.propTypes = {
  /* eslint react/forbid-prop-types: 0 */
  google: PropTypes.object.isRequired,
  address: PropTypes.string.isRequired,
  state: PropTypes.string.isRequired,
  zip: PropTypes.string.isRequired,
  city: PropTypes.string.isRequired,
  lat: PropTypes.number.isRequired,
  lng: PropTypes.number.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func
  })
};

SingleMapDetail.defaultProps = {
  history: {}
};

const SinglePropertyMapWrapper = GoogleApiWrapper({
  apiKey: process.env.REACT_APP_GOOGLE_MAPS_KEY
})(SingleMapDetail);

export default SinglePropertyMapWrapper;
