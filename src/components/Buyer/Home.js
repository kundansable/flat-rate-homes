import React from 'react';
import Header from './common/Header';
import NewlyAddedProperties from './NewlyAddedProperties';
import RefundSlider from './RefundSlider';

import Traditional from '../../theme/Buyer/images/traditionalway.png';
import VsComapre from '../../theme/Buyer/images/vs.png';
import LogoImage from '../../theme/Buyer/images/logo-img.png';
import Footer from './common/Footer';
// import FeaturedHome from './FeaturedHome';

import ReviewStar from '../../theme/Buyer/images/review-star.jpeg';
import SearchPropertyBtn from './common/SearchPropertyBtn';

export default function Home() {
  return (
    <>
      <Header />
      {/* Banner */}
      <div className="home-banner">
        <div className="parallax">
          <div className="parallax-overlay" />
          <div className="parallax-content ">
            <div className="container">
              <div className="row">
                <div className="col-md-12">
                  <div className="main-search-container">
                    <h2>Find Your Dream Home</h2>
                    <SearchPropertyBtn />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="container newly-added">
        <div className="row">
          <div className="col-md-12">
            <h1 className="boxtittle">Newly Added</h1>
          </div>
        </div>
      </div>

      <NewlyAddedProperties />

      <div className="biggest-refund">
        <div className="container">
          <h1 className="boxtittle pb-1">
            How Much Can You Save With Flat Rate Homes?
          </h1>
          <div className="how-contend">
            With Flat Rate Homes premier buyer program you can save thousands by
            reducing the purchase price of the home or getting up to 100% of the
            buyer agent commission rebated back to you. You also get
            professional full offer support from our expert agents through the
            entire process! Contact us now!
          </div>

          <div className="cal-slider">
            <div className="column text-center">
              <RefundSlider />
            </div>
          </div>
        </div>
      </div>

      <div className="parallax simplicity-is-key margin-bottom-70">
        <div className="parallax-overlay" />
        <div className="text-content white-font">
          <div className="container">
            <div className="row">
              <div className="col-12">
                <h1 className="boxtittle">
                  Simplicity is Key. Here are the steps.
                </h1>
              </div>
            </div>

            <div className="row">
              <ul className="col-12">
                <li className="icon-box-1 alternative">
                  <div className="icon-container">
                    {/* <i className="im im-icon-Archery" /> */}
                    <i className="im im-icon-Yes" />
                  </div>

                  <h3>SHOP LIKE A PRO</h3>
                  <p>
                    Access to the biggest database, with the most up to date
                    information so you have all the facts that most people don’t
                    know.
                  </p>
                </li>
                {/* <li className="icon-box-1 alternative">
                  <div className="icon-container">
                    <i className="im im-icon-Cursor-Click2" />
                  </div>

                  <h3>CLICK TO SEE</h3>
                  <p>
                    Scheduling showing has never been easier. Book your visits
                    with a click of a button at times that work best for you.
                    You can even see available open houses, virtual tours.
                  </p>
                </li> */}
                <li className="icon-box-1 alternative">
                  <div className="icon-container">
                    <i className="im im-icon-Idea" />
                  </div>

                  <h3>OFFERS MADE EZ</h3>
                  <p>
                    From submitting to negotiations, we have your back. Our EZ
                    submission tool allows you to submit and digitally sign
                    legal professional offers in minutes.
                  </p>
                </li>
                <li className="icon-box-1 alternative">
                  <div className="icon-container">
                    <i className="im im-icon-Target-Market" />
                  </div>

                  <h3>EXPERT NEGOTIATORS</h3>
                  <p>
                    WWith award winning support, our team of real estate experts
                    will help throughout the process to make sure you get the
                    best price and the biggest bang for your buck.
                  </p>
                </li>
                <li className="icon-box-1 alternative">
                  <div className="icon-container">
                    <i className="im im-icon-Save" />
                  </div>

                  <h3>SAVE THOUSANDS</h3>
                  <p>
                    With our premier buyer program you can get up to 100% of the
                    offered commission back as a cash rebate or reduce the price
                    of the home by up to 3%.
                  </p>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div className="dare-to-comapre">
        <div className="container">
          <div className="row">
            <div className="col-12">
              <h1 className="boxtittle ">Dare to Comapre</h1>
            </div>
          </div>
          <div className="for-desktop  mx-auto text-center">
            <div className="row heading-row ">
              <div className="col-4">
                <div className="top-logo">
                  <img src={Traditional} alt="" className="" />
                  <h5>Traditional Way</h5>
                </div>
                <ul className="orenge-text">
                  <li className="list-heading gray">0%</li>
                  <li>Yes</li>
                  <li>No</li>
                  <li>No</li>
                  <li>No</li>
                  <li>No</li>
                  <li>No</li>
                </ul>
              </div>
              <div className="col-4">
                <div className="top-logo">
                  <img src={VsComapre} alt="" className="pt-3" />
                </div>
                <div className="shadow-box">
                  <ul>
                    <li>
                      <h5>Buyer agent commission refund*</h5>
                    </li>
                    <li>
                      <h5>Dedicated agent</h5>
                    </li>{' '}
                    <li>
                      <h5>
                        Specially trained offer specialists for negotiation and
                        contract support
                      </h5>
                    </li>{' '}
                    <li>
                      <h5>Award winning customer support</h5>
                    </li>
                    <li>
                      <h5>Online real estate assistance</h5>
                    </li>
                    <li>
                      <h5>Free unlimited home search</h5>
                    </li>
                    <li>
                      <h5>Free Concierge Service</h5>
                    </li>
                  </ul>
                </div>
              </div>

              <div className="col-4">
                <div className="top-logo">
                  <img src={LogoImage} alt="" className="logo-img pt-3" />
                </div>
                <ul className="red-text">
                  <li className="list-heading white">100%</li>
                  <li>Yes</li>
                  <li>Yes</li>
                  <li>Yes</li>
                  <li>Yes</li>
                  <li>Yes</li>
                  <li>Yes</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* <div className="container feature-home">
        <div className="row">
          <div className="col-md-12">
            <h1 className="boxtittle">Featured Homes</h1>
          </div>
        </div>
      </div> */}

      {/* <FeaturedHome /> */}

      <div className="container review-block">
        <div className="row">
          <div className="col-md-12">
            <h1 className="boxtittle">Check Out Our Reviews</h1>
          </div>
        </div>
        <div className="row">
          <div className="col-md-4">
            <div className="review-card">
              <div className="review-text">
                Flat Rate made the home selling process effortless and
                comfortable. Not only that but they saved us $1000s in
                commissions
              </div>
              <div className="row">
                <div className="col-6 ">
                  <img src={ReviewStar} alt="" className=" review-star" />{' '}
                </div>
                <div className="col-6 text-right">
                  <div className="review-name">Stephen Swensen </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <div className="review-card">
              <div className="review-text">
                Such a great experience selling our home with Flat Rate Homes.
                Keith Callahan was such a great support with every detail.
              </div>
              <div className="row">
                <div className="col-6 ">
                  <img src={ReviewStar} alt="" className="review-star" />{' '}
                </div>
                <div className="col-6 text-right">
                  <div className="review-name">Lori White</div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <div className="review-card">
              <div className="review-text">
                They made selling our home so easy, communication was great, we
                highly recommend Keith and Flat Rate Homes.
              </div>
              <div className="row">
                <div className="col-6 ">
                  <img src={ReviewStar} alt="" className="review-star" />{' '}
                </div>
                <div className="col-6 text-right">
                  <div className="review-name">Shastina Carter </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <section className="fullwidth margin-top-105 mb-0">
        {/* <!-- Accordion --> */}
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <h1 className="boxtittle pb-5">
                Any questions? We are here to help
              </h1>
            </div>
          </div>
          <div className="row mt-4">
            <div className="col-md-6 ">
              <div className="style-1 fp-accordion mt-0">
                <div className="accordion" id="accordion">
                  <div className="card">
                    <div className="card-header" id="headingOne">
                      <h3 className="mb-0">
                        <button
                          type="button"
                          className="btn btn-link"
                          data-toggle="collapse"
                          data-target="#collapseOne"
                          aria-expanded="true"
                          aria-controls="collapseOne"
                        >
                          So, what’s the catch?{' '}
                          <i className="fa fa-angle-down" />{' '}
                        </button>
                      </h3>
                    </div>

                    <div
                      id="collapseOne"
                      className="collapse show"
                      aria-labelledby="headingOne"
                      data-parent="#accordion"
                    >
                      <div className="card-body">
                        <p>
                          There is none! By using our technology and working
                          through our system, you’re able to do what a
                          traditional agent does without paying an arm and a
                          leg. Our online system and dedicated agents make it
                          easy to make offers on not only our listings, but any
                          listing on the MLS. Then we help you throughout the
                          entire deal to make sure your offer and contract goes
                          through smoothly and stress free. If you found the
                          home why pay thousands to an agent right? It just
                          makes sense to us.
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-header" id="headingTwo">
                      <h3 className="mb-0">
                        <button
                          type="button"
                          className="btn btn-link collapsed"
                          data-toggle="collapse"
                          data-target="#collapseTwo"
                          aria-expanded="false"
                          aria-controls="collapseTwo"
                        >
                          How do I find my new home?{' '}
                          <i className="fa fa-angle-down" />
                        </button>
                      </h3>
                    </div>
                    <div
                      id="collapseTwo"
                      className="collapse"
                      aria-labelledby="headingTwo"
                      data-parent="#accordion"
                    >
                      <div className="card-body">
                        <p>
                          Searching for your new dream home have never been
                          easier. Just click on our our property search feature
                          away you go. Then once you find the property you are
                          looking for you can submit an offer in a few simple
                          steps. When searching through our site you are
                          searching the MLS and using the same resources that
                          agents use when searching for homes.
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-header" id="headingThree">
                      <h3 className="mb-0">
                        <button
                          type="button"
                          className="btn btn-link collapsed"
                          data-toggle="collapse"
                          data-target="#collapseThree"
                          aria-expanded="false"
                          aria-controls="collapseThree"
                        >
                          How does the 100% commission refund work?
                          <i className="fa fa-angle-down" />
                        </button>
                      </h3>
                    </div>
                    <div
                      id="collapseThree"
                      className="collapse"
                      aria-labelledby="headingThree"
                      data-parent="#accordion"
                    >
                      <div className="card-body">
                        <p>
                          Buyer agents typically collect a 3% commission on the
                          home’s selling price. This is paid for by the listing
                          agent and/or homeowner. By working with Flat Rate
                          Homes once you find a home your are wanting to submit
                          an offer on, we become your broker and will collect
                          the 3% commission being offered on the MLS when the
                          contract closes. Then we refund it back to you, minus
                          our flat fee of $950. Essentially when using our
                          online offer service we will refund you any
                          commissions paid to us that are above our $950 minimum
                          commission. Properties that are For Sale By Owner do
                          not apply but give us a call and ask about our FSBO
                          service to still save thousands.
                        </p>
                        <p>
                          *Note that our refund is dependent on a minimum
                          commission of $950 and your lenders and/or builders
                          approval is required. There also may be restrictions
                          with VA and FHA loans. If you are ready to go though,
                          before you talk to the builder and/or a lender give us
                          a call and we are happy to help you come up with a
                          plan to make sure you get the maximum refund back.
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-header" id="headingFour">
                      <h3 className="mb-0">
                        <button
                          type="button"
                          className="btn btn-link collapsed"
                          data-toggle="collapse"
                          data-target="#collapseFour"
                          aria-expanded="false"
                          aria-controls="collapseFour"
                        >
                          How do I qualify for the 100% refund?{' '}
                          <i className="fa fa-angle-down" />
                        </button>
                      </h3>
                    </div>
                    <div
                      id="collapseFour"
                      className="collapse"
                      aria-labelledby="headingFour"
                      data-parent="#accordion"
                    >
                      <div className="card-body">
                        <p>
                          First, once you find the home you want to make an
                          offer on you can submit an offer through our website,
                          and we will handle the rest. During this process, in
                          order to qualify, you will need to sign a Buyer Broker
                          Agreement (BBA) which we will allow us to legally
                          represent you during the transaction and be able to
                          collect the offered commission. That means you cannot
                          have another agreement in place with another agent
                          outside of Flat Rate Homes. This is the only way,
                          submit offers on your behalf and receive the
                          commission.
                        </p>
                        <p>
                          *Refunds do not apply toward For Sale By Owner (FSBO)
                          direct deals.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="style-1 fp-accordion mt-0">
                <div className="accordion" id="accordion">
                  <div className="card">
                    <div className="card-header" id="headingFive">
                      <h3 className="mb-0">
                        <button
                          type="button"
                          className="btn btn-link collapsed"
                          data-toggle="collapse"
                          data-target="#collapseFive"
                          aria-expanded="true"
                          aria-controls="collapseFive"
                        >
                          How & when do I get my refund?{' '}
                          <i className="fa fa-angle-down" />{' '}
                        </button>
                      </h3>
                    </div>

                    <div
                      id="collapseFive"
                      className="collapse"
                      aria-labelledby="headingFive"
                      data-parent="#accordion"
                    >
                      <div className="card-body">
                        <p>
                          There are many ways refunds can be given back and used
                          and we can help customize the refund to best meet your
                          needs and help the transaction go through. Lenders and
                          builders might also have restrictions of what
                          commissions refunds can be used for. this is why we
                          will help you through the process to make sure you you
                          get the best possible refund and using it in the best
                          possible way. The most common way people use their
                          refund though are as follows:
                        </p>
                        <ol>
                          <li>
                            You can have the refund immediately go toward a
                            price reduction so you can make a more competitive
                            offer or just reduce the purchase price all
                            together.
                          </li>
                          <li>
                            you can get a check written for the refund after the
                            home has closed.
                          </li>
                          <li>
                            There are hundreds of other ways we can structure it
                            as well
                          </li>
                        </ol>
                      </div>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-header" id="headingSix">
                      <h3 className="mb-0">
                        <button
                          type="button"
                          className="btn btn-link collapsed"
                          data-toggle="collapse"
                          data-target="#collapseSix"
                          aria-expanded="false"
                          aria-controls="collapseSix"
                        >
                          What do I tell a listing agent when they ask if I’m
                          represented by an agent?
                          <i className="fa fa-angle-down" />
                        </button>
                      </h3>
                    </div>
                    <div
                      id="collapseSix"
                      className="collapse"
                      aria-labelledby="headingSix"
                      data-parent="#accordion"
                    >
                      <div className="card-body">
                        {' '}
                        <p>
                          If you are going to make an offer through our site,
                          Yes, you are! You’re represented by Flat Rate Homes.
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-header" id="headingSeven">
                      <h3 className="mb-0">
                        <button
                          type="button"
                          className="btn btn-link collapsed"
                          data-toggle="collapse"
                          data-target="#collapseSeven"
                          aria-expanded="false"
                          aria-controls="collapseSeven"
                        >
                          What happens if I don’t close on the property?
                          <i className="fa fa-angle-down" />
                        </button>
                      </h3>
                    </div>
                    <div
                      id="collapseSeven"
                      className="collapse"
                      aria-labelledby="headingSeven"
                      data-parent="#accordion"
                    >
                      <div className="card-body">
                        {' '}
                        <p>
                          For our buyer refund program, our $950 fee comes from
                          the traditional agent commission that’s paid upon
                          closing. So if you don’t close, you don’t have to pay
                          us the $950, $0, zip, nada.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>{' '}
              </div>
            </div>
          </div>
        </div>
      </section>

      <span className="flip-banner browse-properties">
        <div className="flip-banner-content">
          <h2 className="flip-visible">
            We help people and homes find each other
          </h2>
          <h2 className="flip-hidden">
            Browse Properties <i className="sl sl-icon-arrow-right" />
          </h2>
        </div>
      </span>

      <Footer />
    </>
  );
}
