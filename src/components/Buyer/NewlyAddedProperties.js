import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import NumberFormat from 'react-number-format';
import ItemsCarousel from 'react-items-carousel';

import PageLoader from '../../common/PageLoader';
import getNewlyAddedProperties from '../../api/BuyerApi/NewlyAddedPropertiesApi';
import { PATH_BUYER_PROPERTY_DETAIL } from '../../constants';

export default function NewlyAddedProperties() {
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState(null);
  const [activeItemIndex, setActiveItemIndex] = useState(0);

  const getNewlyAddedList = () => {
    getNewlyAddedProperties().then(response => {
      setData(response);
      setIsLoading(false);
    });
  };
  useEffect(() => {
    if (isLoading) {
      getNewlyAddedList();
    }
  }, [isLoading]);

  return (
    <>
      {isLoading ? (
        <div className="text-center">
          <PageLoader />
        </div>
      ) : (
        <>
          {data && data.length === 0 ? (
            <div className="text-center">Data not available.</div>
          ) : (
            <ItemsCarousel
              infiniteLoop
              requestToChangeActive={setActiveItemIndex}
              activeItemIndex={activeItemIndex}
              numberOfCards={3}
              gutter={20}
              leftChevron={
                <button type="button" className="left-arrowd">
                  {'<'}
                </button>
              }
              rightChevron={
                <button type="button" className="right-arrowd">
                  {'>'}
                </button>
              }
            >
              {data &&
                data.map(detail => (
                  <div
                    className="carousel home-carousel"
                    key={detail.ListingKey}
                  >
                    <Link
                      to={PATH_BUYER_PROPERTY_DETAIL.replace(
                        ':id',
                        detail.ListingKey
                      )}
                    >
                      <div className="carousel-item">
                        <div className="listing-item">
                          <span
                            href="single-property-page-1.html"
                            className="listing-img-container"
                          >
                            <div className="listing-img-content">
                              <span className="listing-price">
                                <NumberFormat
                                  value={detail.ListPrice || 'N/A'}
                                  displayType="text"
                                  thousandSeparator
                                  prefix="$"
                                />
                              </span>
                            </div>

                            <div className="listing-carousel">
                              <div>
                                <img
                                  src={
                                    detail &&
                                    detail.Media &&
                                    detail.Media[0] &&
                                    detail.Media[0].MediaURL
                                  }
                                  alt="media"
                                />
                              </div>
                            </div>
                          </span>

                          <div className="listing-content">
                            <div className="listing-title">
                              <h4>
                                <span href="single-property-page-1.html">
                                  {detail.StreetName}
                                </span>
                              </h4>
                              <span className="listing-address popup-gmaps">
                                <i className="fa fa-map-marker" />
                                {detail.Address || 'N/A'}
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Link>
                  </div>
                ))}
            </ItemsCarousel>
          )}
        </>
      )}
    </>
  );
}
