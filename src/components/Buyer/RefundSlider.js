import React, { useState } from 'react';

import InputRange from 'react-input-range';
import NumberFormat from 'react-number-format';

import 'react-input-range/lib/css/index.css';

export default function RefundSlider() {
  const [sliderValue, setSliderValue] = useState(150000);

  const onSliderChange = value => {
    setSliderValue(value);
  };

  const calculateRefund = (value, key) => {
    const refundValue = Math.round(value * 0.03);
    return (
      <NumberFormat
        value={key === 'refund' ? refundValue : refundValue + 10000}
        displayType="text"
        thousandSeparator
        prefix="$"
      />
    );
  };

  return (
    <>
      <div className="slidecontainer">
        <div className="valuebox">
          Purchase price:{' '}
          <NumberFormat
            value={sliderValue}
            displayType="text"
            thousandSeparator
            prefix="$"
          />
        </div>
      </div>
      <InputRange
        minValue={100000}
        maxValue={1000000}
        value={sliderValue}
        step={5000}
        onChange={onSliderChange}
      />
      <div className="column text-center">
        <table className="table-pricing unstriped margin-top-small margin-bottom-medium">
          <thead>
            <tr>
              <th className="visibility-hidden">&nbsp;</th>
              <th className="table-pricing-light text-center">
                Traditional Way
              </th>
              <th className="table-pricing-accent text-center">
                With Flat Rate Homes
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td className="text-left">
                Buyer refund when you by through Flat Rate Homes
              </td>
              <td className="contend">$0</td>
              <td className="table-pricing-accent-light">
                <span id="refundAmount">
                  {calculateRefund(sliderValue, 'refund')}
                </span>
                *
              </td>
            </tr>
            <tr>
              <td className="text-left color-black">
                Average amount saved if selling through Flat Rate Homes
              </td>
              <td className="contend">$0</td>
              <td className="table-pricing-accent-light">
                $<span id="loanSavingsAmount">10,000</span>
              </td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <td className="table-pricing-light text-left">Total Savings</td>
              <td className="table-pricing-dark">$0</td>
              <td className="table-pricing-accent">
                <span id="totalSavingsAmount">
                  {calculateRefund(sliderValue, 'total')}
                </span>
              </td>
            </tr>
          </tfoot>
        </table>
        <p className="fineprint margin-top-small mt-4">
          *Refund shown is based on a 3% Buyer Agent Commission (BAC). Refunds
          are subject to sold price, agent commission listed on MLS, market
          conditions and are subject to a minimum commission of $950. Refunds
          may vary.
        </p>
      </div>
    </>
  );
}
