/* eslint-disable react/forbid-prop-types */
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { Map, GoogleApiWrapper, Marker, InfoWindow } from 'google-maps-react';
import queryString from 'query-string';
import Select from 'react-select';
import NumberFormat from 'react-number-format';
import moment from 'moment';

import PageLoader from '../../common/PageLoader';
import Header from './common/Header';
import getMlsListingData from '../../api/BuyerApi/PropertyListApi';
import MapSelectFilters from '../../constants/Buyer/MapFilters';
import {
  PATH_BUYER_PROPERTY_DETAIL,
  PATH_BUYER_PROPERTY_LIST
} from '../../constants/index';
import Toastr from '../../common/Toastr';
import SearchPropertyBtn from './common/SearchPropertyBtn';

const mapStyles = {
  width: '100%',
  height: '100%'
};

const customStyles = {
  menu: (provided, state) => ({
    ...provided,
    borderBottom: '1px dotted pink',
    color: state.selectProps.primaryColor
  }),

  option: provided => ({
    ...provided,
    cursor: 'pointer',
    color: '#000000',
    backgroundColor: '#ffffff',
    ':hover': {
      backgroundColor: '#F84225',
      color: '#ffffff'
    }
  })
};

class BuyerPropertyListComponent extends Component {
  constructor(props) {
    super(props);
    this.selectedPropertRef = React.createRef();
    this.qParams = queryString.parse(props.history.location.search);
    this.selectedFilters = {};
    this.state = {
      zoomLevel: 12,
      center: {
        lat: parseFloat(this.qParams.latitude),
        lng: parseFloat(this.qParams.longitude)
      },
      isLoading: false,
      selectedProperty: [],
      mlsList: [],
      northEast: {},
      southWest: {},
      northWest: {},
      southEast: {},
      isZoomChanged: false,
      needToRefresh: false,
      showMbViewData: 'map',
      propertyImg: ''
    };
  }

  componentDidMount() {
    const params = this.qParams;
    this.selectedFilters = this.qParams;
    const queryParams = { ...params, page: 1 };
    this.updateQueryParams(queryParams);
    this.getData(this.getQueryParams());
  }

  fetchResult = () => {
    const { history } = this.props;
    const params = queryString.parse(history.location.search);
    this.updateQueryParams({ ...params, page: 1 });
    this.getData(this.getQueryParams());
    this.setState({
      center: {
        lat: parseFloat(this.selectedFilters.latitude),
        lng: parseFloat(this.selectedFilters.longitude)
      }
    });
  };

  updateQueryParams = queryParams => {
    this.selectedFilters = queryParams;
  };

  getData = queryParams => {
    this.changeLoaderFlag(true);
    getMlsListingData(queryParams)
      .then(res => {
        this.changeLoaderFlag(false);
        this.setState({ mlsList: res.data, needToRefresh: false });
      })
      .catch(() => {
        this.changeLoaderFlag(false);
      });
  };

  changeLoaderFlag = flag => {
    this.setState({ isLoading: flag });
  };

  onMarkerClick = (props, marker, e, data, imgUrl) => {
    const { mlsList } = this.state;
    this.setState({
      selectedPlace: data,
      propertyImg: imgUrl,
      activeMarker: marker,
      showingInfoWindow: true
    });
    const selectedPrty = mlsList.filter(store => {
      if (store.ListingKey === props.id) {
        return store;
      }
      return null;
    });
    this.setState({ selectedProperty: selectedPrty });

    this.selectedPropertRef.current.scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    });
  };

  displayMarkers = () => {
    const { mlsList } = this.state;
    return Array.isArray(mlsList) ? (
      mlsList.length &&
        mlsList.map(store => {
          return (
            <Marker
              key={Math.random()}
              id={store.ListingKey}
              title={store.Address}
              position={{
                lat: store.GeoLocation.latitude,
                lng: store.GeoLocation.longitude
              }}
              onClick={(props, marker, e) =>
                this.onMarkerClick(
                  props,
                  marker,
                  e,
                  store.Address,
                  store.Media && store.Media[0] && store.Media[0].MediaURL
                )
              }
            />
          );
        })
    ) : (
      <Toastr type="error" message="Error!" />
    );
  };

  viewMoreProperties = () => {
    const params = this.getQueryParams();

    const pageNumber = parseInt(params.page, 10);
    const queryParams = { ...params, page: pageNumber + 1 };
    this.updateQueryParams(queryParams);

    if (pageNumber) {
      this.getData(this.getQueryParams());
    }
  };

  closeSelectedProperty = () => {
    this.setState({ selectedProperty: [] });
  };

  displayAddress = propertyList => {
    const { selectedProperty } = this.state;

    if (Array.isArray(propertyList)) {
      return propertyList.map(store => {
        return (
          <div
            className="col-xl-6 col-lg-12 col-md-6 col-sm-6 col-12"
            key={Math.random()}
            id={store.ListingKey}
          >
            <Link
              to={PATH_BUYER_PROPERTY_DETAIL.replace(':id', store.ListingKey)}
              target="_blank"
            >
              <div className="listing-item ">
                <span
                  href="single-property-page-1.html"
                  className={
                    selectedProperty &&
                    selectedProperty[0] &&
                    selectedProperty[0].ListingKey === store.ListingKey
                      ? 'listing-img-container selected-location'
                      : 'listing-img-container'
                  }
                >
                  <div className="listing-img-content">
                    <span className="listing-price">
                      <NumberFormat
                        value={store.ListPrice}
                        displayType="text"
                        thousandSeparator
                        prefix="$"
                      />
                      <i>{store.LivingArea} / sq ft</i>
                    </span>
                  </div>

                  <div className="listing-carousel">
                    <div>
                      <img
                        src={
                          store.Media &&
                          store.Media[0] &&
                          store.Media[0].MediaURL
                        }
                        alt=""
                      />
                    </div>
                  </div>
                </span>

                <div className="listing-content">
                  <div className="listing-title">
                    <h4>
                      <i className="fa fa-map-marker mr-2" />
                      <span href="single-property-page-1.html">
                        {store.Address}
                      </span>
                    </h4>

                    <span
                      href="single-property-page-1.html"
                      className="details button border"
                    >
                      Details
                    </span>
                  </div>

                  <ul className="listing-details">
                    <li>{store.LivingArea} sq ft</li>
                    <li>
                      {store.BedroomsTotal || 'N/A'}{' '}
                      {store.BedroomsTotal > 1 ? 'Bedrooms' : 'Bedroom'}
                    </li>
                    <li>
                      {store.RoomsTotal || 'N/A'}{' '}
                      {store.RoomsTotal > 1 ? 'Rooms' : 'Room'}
                    </li>
                    <li>
                      {store.BathroomsFull || 'N/A'}{' '}
                      {store.BathroomsFull > 1 ? 'Bathrooms' : 'Bathroom'}{' '}
                    </li>
                  </ul>

                  <div className="listing-footer">
                    <div className="row">
                      <div className="col-6">
                        <span href="#" className="">
                          <i className="fa fa-user" />{' '}
                          {store.ListAgentFullName || 'N/A'}
                        </span>
                      </div>
                      <div className="col-6 text-right">
                        <span className="">
                          <i className="fa fa-calendar-o" />{' '}
                          {moment(store.ModificationTimestamp).fromNow() ||
                            'N/A'}
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Link>
          </div>
        );
      });
    }
    return <Toastr type="error" message="Error!" />;
  };

  centerMoved = (mapProps, map) => {
    const query = this.getQueryParams();
    const q = {
      ...query,
      latitude: map.getCenter().lat(),
      longitude: map.getCenter().lng()
    };
    this.updateQueryParams(q);
    this.setState({ isZoomChanged: false, needToRefresh: true });
  };

  onMapReady = (mapProps, map, _this) => {
    this.map = map;
    const { google } = this.props;
    const { zoomLevel } = this.state;

    let zoom = zoomLevel;
    google.maps.event.addListener(
      map,
      'zoom_changed',
      function customZoomEvent() {
        zoom = map.getZoom();

        const zoomCenter = {
          lat: map.center.lat(),
          lng: map.center.lng()
        };

        _this.setState({ zoomLevel: zoom, center: zoomCenter });

        const bounds = map.getBounds();

        const northEast = bounds.getNorthEast(); // LatLng of the north-east corner
        const southWest = bounds.getSouthWest(); // LatLng of the south-west corder
        const northWest = new google.maps.LatLng(
          northEast.lat(),
          southWest.lng()
        );
        const southEast = new google.maps.LatLng(
          southWest.lat(),
          northEast.lng()
        );
        _this.setState({ northEast, southWest, northWest, southEast });
        _this.setState({ isZoomChanged: true, needToRefresh: true });
      }
    );
  };

  getQueryParams = () => {
    return this.selectedFilters;
  };

  mapDefaultChoiceValue = (value, options) => {
    if (value && value.value) {
      return options.find(option => option.value === value.value);
    }
    return options.find(option => option.value === value);
  };

  handleAuthorChange = (event, field) => {
    if (event.target) {
      // console.log(event.target.name, event.target.value);
    } else {
      const query = this.getQueryParams();
      const q = { ...query, [field.name]: event.value };
      this.updateQueryParams(q);
    }
  };

  onUpdateClick = () => {
    const { history } = this.props;
    const params = this.getQueryParams();
    delete params.page;
    const query = { ...params, page: 1 };
    const stringified = queryString.stringify(query);
    history.push({
      pathname: `${PATH_BUYER_PROPERTY_LIST}`,
      search: `${stringified}`
    });
    this.getData(query);
  };

  onClearFilterClick = () => {
    const params = this.getQueryParams();
    const { q, page, latitude, longitude } = params;
    this.updateQueryParams({ q, page, latitude, longitude });

    this.onUpdateClick();
  };

  onCheckClick = checked => {
    const query = this.getQueryParams();
    const q = { ...query, [checked.target.name]: checked.target.checked };
    this.updateQueryParams(q);
  };

  redirctToHome = () => {
    const { history } = this.props;
    history.push('/home');
  };

  onMapRefresh = () => {
    const {
      isZoomChanged,
      northEast,
      southWest,
      northWest,
      southEast
    } = this.state;
    this.closeSelectedProperty();
    if (isZoomChanged) {
      const q = this.getQueryParams();

      const boundaries = [
        `${northEast.lng()} ${northEast.lat()}`,
        `${southWest.lng()} ${southWest.lat()}`,
        `${northWest.lng()} ${northWest.lat()}`,
        `${southEast.lng()} ${southEast.lat()}`,
        `${northEast.lng()} ${northEast.lat()}`
      ];
      const query = {
        ...q,
        boundaries
      };
      this.getData(query);
    } else {
      const q = this.getQueryParams();
      this.getData(q);
    }
  };

  showMap = (
    google,
    mlsList,
    activeMarker,
    showingInfoWindow,
    selectedPlace,
    propertyImg
  ) => {
    const { zoomLevel, needToRefresh, center, showMbViewData } = this.state;
    return (
      <div className="col-lg-6 pl-lg-0">
        <div
          className={
            showMbViewData === 'map'
              ? 'active-panel map-panel'
              : 'deactive-panel map-panel'
          }
        >
          <div className="fs-inner-container">
            <div id="map-container">
              <div id="mapsd">
                <Map
                  google={google}
                  centerAroundCurrentLocation
                  zoom={zoomLevel}
                  style={mapStyles}
                  initialCenter={center}
                  center={center}
                  onDragend={this.centerMoved}
                  onZoomChanged={this.handleZoomChanged}
                  onReady={(mapProps, map) =>
                    this.onMapReady(mapProps, map, this)
                  }
                >
                  {this.displayMarkers()}
                  <InfoWindow
                    className="marker-window"
                    onClose={this.closeSelectedProperty}
                    marker={activeMarker}
                    visible={showingInfoWindow}
                  >
                    <div className="selected-place">
                      <div className="row m-auto">
                        <div className="col-4">
                          <img src={propertyImg} alt="" />
                        </div>
                        <div className="col-8">{selectedPlace}</div>
                      </div>
                    </div>
                  </InfoWindow>
                </Map>
              </div>

              <ul id="mapnav-buttons" className="top">
                <li>
                  <button
                    onClick={this.onMapRefresh}
                    className={needToRefresh ? 'button' : 'button with-bg'}
                    type="button"
                  >
                    Refresh
                  </button>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  };

  displaySelectFilterList = (params, Filters) => {
    return Filters.map(filterData => (
      <div className="col-lg-6 mb-3 select-box" key={filterData.id}>
        <div className="select-input disabled-first-option">
          <span>{filterData.label}</span>
          <Select
            id={filterData.id}
            className="w-350"
            name={filterData.name}
            options={filterData.options}
            styles={customStyles}
            defaultValue={this.mapDefaultChoiceValue(
              params[filterData.name],
              filterData.options
            )}
            onChange={this.handleAuthorChange}
          />
        </div>
      </div>
    ));
  };

  showFilters = params => {
    return (
      <div className="rightside-filter">
        <input type="checkbox" id="toggle" />
        <div className="content text-right">
          <label htmlFor="toggle" className="toggle-btn">
            <i className="sl sl-icon-plus mr-2" />
            Filter
          </label>
        </div>
        <div className="rightsidebar">
          <div className="title-sidebar">
            <div className="row">
              <div className="col-6">
                <h3>Filter</h3>
              </div>
              <div className="col-6">
                <div className="text-right">
                  <label htmlFor="toggle" className="exit">
                    <i className="im im-icon-Close-Window" />
                  </label>
                </div>
              </div>
            </div>
          </div>
          <div className="clearfix" />
          <div className="more-search-options relative mt-3">
            <div className="row with-forms mb-1">
              {this.displaySelectFilterList(params, MapSelectFilters)}
            </div>
            <div className="checkboxes in-row">
              <input
                id="cooling"
                type="checkbox"
                name="cooling"
                defaultChecked={
                  this.selectedFilters.cooling === true ||
                  this.selectedFilters.cooling === 'true'
                }
                onChange={event => this.onCheckClick(event)}
              />
              <label htmlFor="cooling">Air Conditioning </label>

              <input
                id="pool"
                type="checkbox"
                name="pool"
                defaultChecked={
                  this.selectedFilters.pool === true ||
                  this.selectedFilters.pool === 'true'
                }
                onChange={event => this.onCheckClick(event)}
              />
              <label htmlFor="pool">Swimming Pool</label>

              <input
                id="heating"
                type="checkbox"
                name="heating"
                defaultChecked={
                  this.selectedFilters.heating === true ||
                  this.selectedFilters.heating === 'true'
                }
                onChange={event => this.onCheckClick(event)}
              />
              <label htmlFor="heating">Central Heating</label>
            </div>
            <button
              type="button"
              className="button fs-map-btn mr-3 mt-3"
              onClick={this.onUpdateClick}
            >
              Update
            </button>
            <button
              type="button"
              className="button fs-map-btn  mt-3"
              onClick={this.onClearFilterClick}
            >
              Clear
            </button>
          </div>
        </div>
      </div>
    );
  };

  showPropertyList = (mlsList, selectedProperty) => {
    return (
      <div className="fs-content" id="list">
        <div className="row">
          <div className="col-12">
            <div className="listings-container grid-layout fs-listings">
              <div className="row">
                {selectedProperty.length ? (
                  <div
                    className="listings-container grid-layout row fs-listings mt-4"
                    ref={this.selectedPropertRef}
                  >
                    <div className="row">
                      {this.displayAddress(selectedProperty)}
                    </div>
                  </div>
                ) : (
                  ''
                )}

                <div className="col-md-7">
                  <p className="showing-results">
                    Displaying {mlsList.length} results{' '}
                    <button
                      type="button"
                      className="ml-2 button buttontopd"
                      onClick={this.viewMoreProperties}
                    >
                      view more properties &gt;
                    </button>
                  </p>
                </div>

                {this.displayAddress(mlsList)}
              </div>
              <div className="row">
                <div className="col-12">
                  <button
                    type="button"
                    className="button mt-3 mb-3"
                    onClick={this.viewMoreProperties}
                  >
                    view more properties &gt;
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  showHideDataMbview = data => {
    this.setState({ showMbViewData: data });
  };

  render() {
    const { google } = this.props;
    const {
      mlsList,
      selectedPlace,
      activeMarker,
      showingInfoWindow,
      selectedProperty,
      isLoading,
      propertyImg,
      showMbViewData
    } = this.state;
    const params = this.getQueryParams();

    if (Array.isArray(mlsList)) {
      return (
        <>
          <Header />
          <div className="mbview-icons">
            <div className="container">
              <div className="row">
                <div className="col-12 text-right">
                  <button
                    className="button with-bg mr-2"
                    onClick={() => this.showHideDataMbview('map')}
                    type="button"
                  >
                    <i className="sl sl-icon-map " />
                  </button>
                  <button
                    className="button with-bg"
                    onClick={() => this.showHideDataMbview('list')}
                    type="button"
                  >
                    <i className="sl sl-icon-list" />
                  </button>
                </div>
              </div>
            </div>
          </div>

          <div className="container-fluid">
            <div className="row">
              {this.showMap(
                google,
                mlsList,
                activeMarker,
                showingInfoWindow,
                selectedPlace,
                propertyImg
              )}
              <div className="col-lg-6">
                <div className="property-search">
                  <SearchPropertyBtn fetchResult={this.fetchResult} />
                </div>
                {mlsList.length ? (
                  <div
                    className={
                      showMbViewData === 'list'
                        ? 'active-panel'
                        : 'deactive-panel'
                    }
                  >
                    <div className="fs-inner-container">
                      {this.showFilters(params)}
                      {this.showPropertyList(mlsList, selectedProperty, params)}
                    </div>
                  </div>
                ) : (
                  <div className="row">
                    <div className="col-12 pt-5 text-center">
                      <h5>No listings found</h5>
                    </div>
                  </div>
                )}
              </div>
            </div>
            {isLoading ? (
              <div className="page-is-loading">
                <div className="loader-outer">
                  <PageLoader />
                </div>
              </div>
            ) : (
              ''
            )}
          </div>
        </>
      );
    }
    return (
      <>
        <Toastr type="error" message={mlsList} />
        {this.redirctToHome()}
      </>
    );
  }
}

BuyerPropertyListComponent.propTypes = {
  google: PropTypes.object.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
    location: PropTypes.object
  }).isRequired
};

const MapWrapper = GoogleApiWrapper({
  apiKey: process.env.REACT_APP_GOOGLE_MAPS_KEY
})(BuyerPropertyListComponent);

export default MapWrapper;
